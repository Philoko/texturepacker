﻿// Author:  Philipp Okoampah
// Created: 07.11.2014 20:45:52

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using TextureAtlas;
using TexturePacker.Channel;
using TexturePacker.Packing;
using TexturePacker.Utils;

namespace TexturePacker
{
    /// <summary>
    /// AtlasCreator
    /// </summary>
    public class AtlasCreator
    {
        // **************************************************************************
        #region Constants

        private const string PNG_FILE_ENDING = "png";
        private const string TGA_FILE_ENDING = "tga";
        private const string BMP_FILE_ENDING = "bmp";
        private const string GIF_FILE_ENDING = "gif";
        private const string JPEG_FILE_ENDING = "jpeg";
        private const string TIFF_FILE_ENDING = "tiff";

        #endregion

        // **************************************************************************
        #region Member variables

        private readonly string[] m_asImageFileEndings = { PNG_FILE_ENDING, TGA_FILE_ENDING, BMP_FILE_ENDING, GIF_FILE_ENDING, JPEG_FILE_ENDING, TIFF_FILE_ENDING };

        #endregion

        // **************************************************************************
        #region Private functions

        /// <summary>
        /// Loads all images from a given directory, save them as <see cref="Bitmap"/>s, cuts of transparent 
        /// areas of this images, and creates a <see cref="AtlasImage"/> for each of them.
        /// </summary>
        /// <param name="imageDirectoryInfo">The <see cref="DirectoryInfo"/> to load the images from.</param>
        /// <param name="iMaxTextureSize">The maximum size of the images.</param>
        /// <param name="iAlphaTreshold">An treshhold to specify</param>
        /// <param name="images">A <see cref="List{T}"/> of the created <see cref="AtlasImage"/>s as out parameter.</param>
        /// <param name="bitmaps">A <see cref="Dictionary{TKey,TValue}"/> of <see cref="Bitmap"/>s for their image names.</param>
        /// <exception cref="Exception">
        /// If the cut off of the transparent pixels with the given <paramref name="iAlphaTreshold"/> 
        /// of an image leads to an image with no pixels left.
        /// </exception>
        /// <exception cref="Exception">
        /// If the width or height of an image AFTER the cut off is still bigger than <paramref name="iMaxTextureSize"/>.
        /// </exception>
        private void LoadAllImagesFromDirectory(DirectoryInfo imageDirectoryInfo, int iMaxTextureSize,
            int iAlphaTreshold, out List<AtlasImage> images, out Dictionary<string, Bitmap> bitmaps)
        {
            Console.WriteLine("Loading images from: \"" + imageDirectoryInfo.FullName + "\"");

            images = new List<AtlasImage>();
            bitmaps = new Dictionary<string, Bitmap>();

            List<FileInfo> imageFiles = imageDirectoryInfo.GetFiles().Where(IsValidImage).ToList();

            foreach (FileInfo imageFile in imageFiles)
            {
                AtlasImage image = new AtlasImage();
                Bitmap imageBitmap = LoadBitmap(imageFile);

                int iXOffset;
                int iYOffset;

                Bitmap cutoffBitmap = CutOffTransparentAreas(imageBitmap, iAlphaTreshold, out iXOffset, out iYOffset);
                if (cutoffBitmap == null)
                {
                    throw new Exception("The image \"" + imageFile.FullName +
                                        "\" has no opaque pixel with a given alpha threshold of \"" + iAlphaTreshold +
                                        "\".");
                }

                if (cutoffBitmap.Width > iMaxTextureSize || cutoffBitmap.Height > iMaxTextureSize)
                {
                    throw new Exception("This size of the image \"" + imageFile.FullName + "\" is \"" +
                                        imageBitmap.Width + ", " + imageBitmap.Height +
                                        "\" which is bigger than the maximum size of \"" + iMaxTextureSize +
                                        "\" even when alpha pixels has been cut off.");
                }

                image.ImageName = RemoveFileExtension(imageFile.Name);
                image.Width = (short)cutoffBitmap.Width;
                image.Height = (short)cutoffBitmap.Height;
                image.OriginalWidth = (short)imageBitmap.Width;
                image.OriginalHeight = (short)imageBitmap.Height;
                image.XOffset = (short)iXOffset;
                image.YOffset = (short)iYOffset;

                bitmaps.Add(image.ImageName, cutoffBitmap);
                images.Add(image);
            }
        }

        /// <summary>
        /// Checks if the given <see cref="FileInfo"/> has a valid image file extension.
        /// </summary>
        /// <param name="file">The <see cref="FileInfo"/> to check.</param>
        /// <returns><c>True</c> if the image file can be loaded, <c>false</c> otherwise.</returns>
        private bool IsValidImage(FileInfo file)
        {
            return file.Exists && m_asImageFileEndings.Any(sEnding => file.Name.EndsWith("." + sEnding));
        }

        /// <summary>
        /// Loads an image from an file.
        /// </summary>
        /// <param name="file">The <see cref="FileInfo"/> to load the image from.</param>
        /// <returns>The loaded image as <see cref="Bitmap"/>.</returns>
        private Bitmap LoadBitmap(FileInfo file)
        {
            if (file.Name.EndsWith(TGA_FILE_ENDING))
            {
                byte[] aiRawBuffer = File.ReadAllBytes(file.FullName);
                return TargaUtils.ReadTarga(aiRawBuffer);
            }
            return (Bitmap)Image.FromFile(file.FullName);
        }

        /// <summary>
        /// Removes the file extension from a string.
        /// </summary>
        /// <param name="sFileName">The filename to remove the extension from.</param>
        /// <returns>The filename without extension.</returns>
        private string RemoveFileExtension(string sFileName)
        {
            return sFileName.Substring(0, sFileName.LastIndexOf(".", StringComparison.Ordinal));
        }

        /// <summary>
        /// Cuts off the transparent areas of an <see cref="Bitmap"/>.
        /// </summary>
        /// <param name="bitmap">The <see cref="Bitmap"/> to cut off the transparent areas.</param>
        /// <param name="iAlphaTreshold">The alpha value. All pixels which have an lower alpha value than this are treated as transparent.</param>
        /// <param name="iXOffset">How much has been cut off on the left side of the image.</param>
        /// <param name="iYOffset">How much has been cut off on the top side of the image.</param>
        /// <returns>The new cut <see cref="Bitmap"/>.</returns>
        private Bitmap CutOffTransparentAreas(Bitmap bitmap, int iAlphaTreshold, out int iXOffset, out int iYOffset)
        {
            int iOriginalWidth = bitmap.Width;
		    int iOriginalHeight = bitmap.Height;

            int[] aiImageBuffer = new int[iOriginalWidth * iOriginalHeight];
            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    aiImageBuffer[y * bitmap.Width + x] = bitmap.GetPixel(x, y).ToArgb();
                }
            }

		    int iLeft = iOriginalWidth + 1;
		    int iRight = -1;
		    int iUp = iOriginalHeight + 1;
		    int iDown = -1;

		    for (int y = 0; y < iOriginalHeight; y++)
		    {
			    for (int x = 0; x < iOriginalWidth; x++)
			    {
				    int iAlpha = (aiImageBuffer[x + y * iOriginalWidth] >> 24) & 0xFF;     
                    
                    // Is it a opaque pixel? 
                    if (iAlpha > iAlphaTreshold)
				    {
					    if (x > iRight)
					    {
						    iRight = x;
					    }
					    if (x < iLeft)
					    {
						    iLeft = x;
					    }
					    if (y > iDown)
					    {
						    iDown = y;
					    }
					    if (y < iUp)
					    {
						    iUp = y;
					    }
				    }
			    }
		    }
		    int iNewWidth = iRight - iLeft + 1;
		    int iNewHeight = iDown - iUp + 1;
		    if (iNewWidth <= 0 || iNewHeight <= 0)
		    {
		        iXOffset = 0;
		        iYOffset = 0;

                // The image has no opaque pixels
		        return null;
		    }

            Rectangle rect = new Rectangle(iLeft, iUp, iNewWidth, iNewHeight);

            iXOffset = iLeft;
            iYOffset = iUp;
            return bitmap.Clone(rect, bitmap.PixelFormat);
        }

        /// <summary>
        /// Render all passed <see cref="Bitmap"/>s an a single <see cref="Bitmap"/> with the 
        /// passed <see cref="PixelFormat"/> and with the information of the passed 
        /// <see cref="AtlasImageContainer"/>. 
        /// </summary>
        /// <param name="imageContainer">
        /// The <see cref="AtlasImageContainer"/> containing the <see cref="AtlasImage"/>s that are 
        /// related to the <paramref name="bitmaps"/>.
        /// </param> 
        /// <param name="ePixelFormat">
        /// The <see cref="PixelFormat"/> of the target <see cref="Bitmap"/>
        /// .</param>
        /// <param name="bitmaps"
        /// >A <see cref="Dictionary{TKey,TValue}"/> of <see cref="Bitmap"/> for their names. The names 
        /// are related to the names of the <see cref="AtlasImage"/>s of the <paramref name="imageContainer"/>.
        /// </param>
        /// <returns>The <see cref="Bitmap"/> which has all other images on it.</returns>
        private Bitmap RenderTextureImage(AtlasImageContainer imageContainer, PixelFormat ePixelFormat,
            Dictionary<string, Bitmap> bitmaps)
        {
            Bitmap bitmap;
            if (ePixelFormat == PixelFormat.Format8bppIndexed)
            {
                bitmap = new Bitmap(imageContainer.Width, imageContainer.Height, PixelFormat.Format32bppArgb);
            }
            else
            {
                bitmap = new Bitmap(imageContainer.Width, imageContainer.Height, ePixelFormat);
            }

            Graphics g = Graphics.FromImage(bitmap);
            foreach (AtlasImage atlasImage in imageContainer.Images)
            {
                g.DrawImage(bitmaps[atlasImage.ImageName], atlasImage.XPos, atlasImage.YPos,
                    atlasImage.Width, atlasImage.Height);
            }
            g.Dispose();

            return bitmap;
        }

        /// <summary>
        /// Checks if all <see cref="ChannelConnection"/>s are valid.
        /// </summary>
        /// <exception cref="NullReferenceException">If no <see cref="ChannelConnection"/>s has benn set.</exception>
        /// <exception cref="Exception">If one <see cref="ChannelConnection"/> refers to an image that was not defined.</exception>
        private void CheckChannelConnections()
        {
            if (ChannelConnections == null)
            {
                throw new NullReferenceException("No color channel connections has been set.");
            }
            foreach (ChannelConnection channelConnection in ChannelConnections)
            {
                if (!InputImages.Any(inputFolder => inputFolder.Name.Equals(channelConnection.InputImageName)))
                {
                    throw new Exception("No input image with the name \"" + channelConnection.InputImageName +"\" was defined.");
                }
                if (!OutputImages.Any(outputImage => outputImage.Name.Equals(channelConnection.OutputImageName)))
                {
                    throw new Exception("No output image with the name \"" + channelConnection.OutputImageName + "\" was defined.");
                }
            }
        }

        /// <summary>
        /// Saves an image in the passed <see cref="PixelFormat"/> to the passed <see cref="FileInfo"/>.
        /// Note the the <see cref="ImageFormat"/> is read from the file extension of the <paramref name="file"/>.
        /// <seealso cref="GetImageFormatForFileEnding"/>
        /// </summary>
        /// <param name="file">The <see cref="FileInfo"/> to save the image.</param>
        /// <param name="bitmap">The <see cref="Bitmap"/> containing the image data.</param>
        /// <param name="ePixelFormat">The <see cref="PixelFormat"/> of the output image.</param>
        private void SaveImage(FileInfo file, Bitmap bitmap, PixelFormat ePixelFormat)
        {
            Console.WriteLine("Saving image \"" + file + "\"");
            if (file.Name.EndsWith(TGA_FILE_ENDING))
            {
                using (FileStream stream = new FileStream(file.FullName, FileMode.Create))
                {
                    byte[] aiImage = TargaUtils.WriteTarga(bitmap, ePixelFormat, true);
                    stream.Write(aiImage, 0, aiImage.Length);
                }
            }
            else
            {
                string sFileEnding = file.Name.Substring(file.Name.LastIndexOf(".", StringComparison.Ordinal)+1);
                bitmap.Save(file.FullName, GetImageFormatForFileEnding(sFileEnding));
            }
        }

        /// <summary>
        /// Returns the <see cref="ImageFormat"/> of a given file extension.
        /// </summary>
        /// <param name="sFileEnding">The extension of the file.</param>
        /// <returns>The <see cref="ImageFormat"/>.</returns>
        private ImageFormat GetImageFormatForFileEnding(string sFileEnding)
        {
            switch (sFileEnding)
            {
               case PNG_FILE_ENDING:
                    return ImageFormat.Png;
               case BMP_FILE_ENDING:
                    return ImageFormat.Bmp;
               case GIF_FILE_ENDING:
                    return ImageFormat.Gif;
               case JPEG_FILE_ENDING:                    
                    return ImageFormat.Jpeg;
               case TIFF_FILE_ENDING:
                    return ImageFormat.Tiff;
               default:
                    throw new Exception("The image format \"" + sFileEnding +"\" is not supported.");
            }
        }

        /// <summary>
        /// Returns the <see cref="PixelFormat"/> of an <see cref="EColorChannel"/>.
        /// </summary>
        /// <param name="eColorChannel">The <see cref="EColorChannel"/> to get the <see cref="PixelFormat"/> for.</param>
        /// <returns>The <see cref="PixelFormat"/>.</returns>
        private PixelFormat GetPixelFormat(EColorChannel eColorChannel)
        {
            switch (eColorChannel)
            {
                case EColorChannel.A:
                    return PixelFormat.Format8bppIndexed;
                case EColorChannel.RGB:
                    return PixelFormat.Format24bppRgb;
                case EColorChannel.RGBA:
                    return PixelFormat.Format32bppArgb;
            }
            return PixelFormat.DontCare;
        }

        /// <summary>
        /// Creates one or more <see cref="AtlasImageContainer"/> from the passed <see cref="AtlasImage"/>s.
        /// </summary>
        /// <param name="sName">Name of the <see cref="AtlasImageContainer"/> to create.</param>
        /// <param name="iMaxTextureSize">The maximum size of the <see cref="AtlasImageContainer"/>.</param>
        /// <param name="bPowerOfTo">Determines wether only power-of-two sizes should be used.</param>
        /// <param name="images">The list of <see cref="AtlasImage"/>s.</param>
        /// <returns>A list of <see cref="AtlasImageContainer"/>.</returns>
        private List<AtlasImageContainer> CreateAtlasImageContainer(string sName, int iMaxTextureSize, bool bPowerOfTo,
            List<AtlasImage> images)
        {
            if (images.Count == 0)
            {
                return null;
            }

            // Sort the images by their area
            images.Sort(new AtlasImageAreaComparer());

            AStorageSize storageSize = bPowerOfTo
                ? (AStorageSize) new StorageSizePot(iMaxTextureSize)
                : new StorageSizeNPot(iMaxTextureSize);

            ImageStorageManager imageStorageManager = new ImageStorageManager();
            imageStorageManager.PackImages(images, storageSize);

            List<AtlasImageContainer> imageContainer =
                imageStorageManager.Select(imageStorage => imageStorage.AtlasImageContainer).ToList();

            for (int i = 0; i < imageContainer.Count; i++)
            {
                AtlasImageContainer textureImage = imageContainer[i];
                textureImage.Name = sName + "_" + i;
            }

            return imageContainer;
        }

        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// The whole image loading, atlas creation and saving process.
        /// </summary>
        /// <exception cref="Exception">
        /// If multiple input folders are specified and an image is not present in all of the input 
        /// folders or has a different size than it's counter parts after cutting of the transparent areas.
        /// </exception>
        public void Process()
        {
            Console.WriteLine("AtlasCreator begin process " + DateTime.Now);
            Console.WriteLine("MaterialName: \t\t" + MaterialName);
            Console.WriteLine("MaxTextureSize: \t" + MaxTextureSize);
            Console.WriteLine("UsePowerOfTwo: \t\t" + UsePowerOfTwo);
            Console.WriteLine("AlphaThreshold: \t" + AlphaThreshold);
            Console.WriteLine("OutputFormat: \t\t" + OutputFormat);
            Console.WriteLine("OutputFolder: \t\t" + OutputFolder);

            CheckChannelConnections();

            Dictionary<string, List<AtlasImageContainer>> textureImageLists = new Dictionary<string, List<AtlasImageContainer>>();

            foreach (InputImage inputFolder in InputImages)
            {            
                List<AtlasImage> images;
                Dictionary<string, Bitmap> bitmaps;

                LoadAllImagesFromDirectory(new DirectoryInfo(inputFolder.Path), MaxTextureSize, AlphaThreshold, out images, out bitmaps);
                List<AtlasImageContainer> textureImages = CreateAtlasImageContainer(inputFolder.Name, MaxTextureSize, UsePowerOfTwo, images);

                // Check if all single texture atlas images in the different folders with the same name got the same size
                foreach (List<AtlasImageContainer> textureImageList in textureImageLists.Values)
                {
                    for (int i = 0; i < textureImages.Count; i++)
                    {
                        foreach (AtlasImage atlasImage in textureImageList[i])
                        {
                            AtlasImage image =
                                new List<AtlasImage>(textureImages[i]).Find(
                                    t => t.ImageName.Equals(atlasImage.ImageName));

                            if (image == null)
                            {
                                throw new Exception("An image with  \"" + new DirectoryInfo(inputFolder.Path).FullName +
                                                    "\\" + atlasImage.ImageName +
                                                    "\" is not present in every folder or it has a different size than it's counter parts in the other folders after cutting of the transparent areas.");
                            }
                            if (atlasImage.Height != image.Height
                                || atlasImage.Width != image.Width)
                            {
                                throw new Exception("The image \"" + new DirectoryInfo(inputFolder.Path).FullName + "\\" +
                                                    atlasImage.ImageName +
                                                    "\" has a different size than it's counter parts in the other folders after cutting of the transparent areas.");
                            }
                        }
                    }
                }

                foreach (AtlasImageContainer textureImage in textureImages)
                {
                    textureImage.Bitmap = RenderTextureImage(textureImage, PixelFormat.Format32bppArgb, bitmaps);
                }
                textureImageLists.Add(inputFolder.Name, textureImages);
            }
            
            // Image names for the texture atlases
            Dictionary<string, List<string>> textureAtlases = new Dictionary<string, List<string>>();

            // Channel connections                
            Dictionary<string, Dictionary<string, Bitmap>> outputBitmaps = new Dictionary<string, Dictionary<string, Bitmap>>();

            Dictionary<string, PixelFormat> outputBitmapPixelFormats = new Dictionary<string, PixelFormat>();
            foreach (OutputImage outputImage in OutputImages)
            {
                PixelFormat ePixelFormat = GetPixelFormat(outputImage.ColorChannel);
                outputBitmapPixelFormats.Add(outputImage.Name, ePixelFormat);

                // Generate all output bitmaps and apply the channel connections             
                foreach (ChannelConnection channelConnection in ChannelConnections)
                {
                    Dictionary<string, Bitmap> outputBitmap;
                    if (!outputBitmaps.TryGetValue(channelConnection.OutputImageName, out outputBitmap))
                    {
                        outputBitmap = new Dictionary<string, Bitmap>();
                        outputBitmaps.Add(channelConnection.OutputImageName, outputBitmap);
                    }

                    List<AtlasImageContainer> textureImageList = textureImageLists[channelConnection.InputImageName];
                    for (int i = 0; i < textureImageList.Count; i++)
                    {
                        AtlasImageContainer inputTexture = textureImageList[i];   
                        Bitmap bitmap;
                        if (!outputBitmap.TryGetValue(channelConnection.OutputImageName + "_" + i, out bitmap))
                        {
                            bitmap = new Bitmap(inputTexture.Width, inputTexture.Height,
                                ePixelFormat == PixelFormat.Format8bppIndexed
                                    ? PixelFormat.Format32bppArgb
                                    : ePixelFormat);
                            outputBitmap.Add(channelConnection.OutputImageName + "_" + i, bitmap);
                        }
                        channelConnection.Write(inputTexture.Bitmap, bitmap, AlphaThreshold);                        
                     }
                }
            }

            // The output folder
            string sOutputPath = OutputFolder;
            if (!sOutputPath.EndsWith(@"/") || !sOutputPath.EndsWith(@"\"))
            {
                sOutputPath += @"/";
            }

            if (!Directory.Exists(sOutputPath))
            {
                Directory.CreateDirectory(sOutputPath);                
            }

            // Save the output bitmaps
            foreach (KeyValuePair<string, Dictionary<string, Bitmap>> bitmaps in outputBitmaps)
            {
                List<string> textureFileNames;
                if (!textureAtlases.TryGetValue(bitmaps.Key, out textureFileNames))
                {
                    textureFileNames = new List<string>();
                    textureAtlases.Add(bitmaps.Key, textureFileNames);
                }

                foreach (KeyValuePair<string, Bitmap> bitmap in bitmaps.Value)
                {
                    string sFile = bitmap.Key;
                    sFile += "." + OutputFormat;

                    if (!textureFileNames.Contains(sFile))
                    {
                        textureFileNames.Add(sFile);
                    }

                    SaveImage(new FileInfo(sOutputPath + sFile), bitmap.Value, outputBitmapPixelFormats[bitmaps.Key]);
                }
            }

            // Output xml                  
            for (int i = 0; i < textureImageLists.First().Value.Count; i++)
            {            
                Atlas atlas = new Atlas();                
                atlas.Material = MaterialName;
                List<AtlasImage> atlasImages = new List<AtlasImage>();
           
                atlasImages.AddRange(textureImageLists.First().Value[i].Images);

                foreach (KeyValuePair<string, List<string>> textureAtlas in textureAtlases)
                {
                    atlas.AddTexture(textureAtlas.Key, textureAtlas.Value[i]);
                }

                atlasImages.Sort();

                foreach (AtlasImage atlasImage in atlasImages)
                {
                    atlas.AddImage(atlasImage);
                }

                string sFileName = MaterialName + "_" + i + ".atlas.xml";

                using (FileStream fileStream = new FileInfo(sOutputPath + sFileName).Open(FileMode.Create))
                {
                    Console.WriteLine("Generating xml file \"" + sOutputPath + sFileName + "\"");
                    XmlSerializer serializer = new XmlSerializer(typeof(Atlas));
                    serializer.Serialize(fileStream, atlas);
                }
            }
            Console.WriteLine("AtlasCreator end process " + DateTime.Now);
        }

        #endregion

        // **************************************************************************

        #region Properties

        /// <summary>
        /// The <see cref="InputImage"/>s for the <see cref="Process"/>. 
        /// </summary>
        [XmlArray]
        public InputImage[] InputImages { get; set; }

        /// <summary>
        /// The <see cref="OutputImage"/>s for the <see cref="Process"/>. 
        /// </summary>
        [XmlArray]
        public OutputImage[] OutputImages { get; set; }

        /// <summary>
        /// The <see cref="ChannelConnection"/>s. 
        /// </summary>
        [XmlArray]
        public ChannelConnection[] ChannelConnections { get; set; }

        /// <summary>
        /// The alpha threshold for cutting of transparent areas. 
        /// </summary>
        [XmlAttribute]
        public int AlphaThreshold { get; set; }

        /// <summary>
        /// If only power-of-two texture sizes should be used. 
        /// </summary>
        [XmlAttribute]
        public bool UsePowerOfTwo { get; set; }

        /// <summary>
        /// The maximum width and height of a generated texture.
        /// </summary>
        [XmlAttribute]
        public int MaxTextureSize { get; set; }

        /// <summary>
        /// User defined name for the kind of usage of the created atlases.
        /// </summary>
        [XmlAttribute]
        public string MaterialName { get; set; }

        /// <summary>
        /// The output image format as file extension.
        /// </summary>
        [XmlAttribute]
        public string OutputFormat { get; set; }

        /// <summary>
        /// The output folder for the images and atlases.
        /// </summary>
        [XmlAttribute]
        public string OutputFolder { get; set; }

        #endregion
    }
}
