﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 17:13:22

namespace TexturePacker.Packing
{
    /// <summary>
    /// An implementation of <see cref="AStorageSize"/> for power-of-two sizes.
    /// </summary>
    public class StorageSizePot : AStorageSize
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// Determines whether the width and height should be swapped in the next increment.
        /// </summary>
        protected bool m_bSwapWidthHeight;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Constructs a new instance with the maximum size. 
        /// </summary>
        /// <param name="iMaxSize">The maximum width or height.</param>
        public StorageSizePot(int iMaxSize) : base(iMaxSize)
        {

        }

        #endregion

        // **************************************************************************
        #region Overwritten from ATextureSize

        /// <summary>
        /// Increments the width or height to the next power-of-two constraint level.
        /// </summary>
        public override void Increment()
        {
            // Swap height and width if it's not square.
            // This is to allow for better fitting of width and tall images
            if (m_iHeight > m_iWidth && m_bSwapWidthHeight)
            {
                int temp = m_iHeight;
                m_iHeight = m_iWidth;
                m_iWidth = temp;
            }
            else if (m_iHeight > m_iWidth)
            {
                m_iWidth *= 2;
            }
            else
            {
                m_iHeight *= 2;
            }
            m_bSwapWidthHeight = !m_bSwapWidthHeight;
            AdjustMaxSize();
        }

        /// <summary>
        /// Decrements the width or height to the previous power-of-two constraint level.
        /// </summary>
        public override void Decrement()
        {
            if (m_iHeight < m_iWidth)
            {
                m_iWidth /= 2;
            }
            else
            {
                m_iHeight /= 2;
            }
            AdjustMinSize();
        }

        /// <summary>
        /// Sets the width and height to the smallest power-of-two constraint sizes that matches the required area.
        /// </summary>
        /// <param name="iRequiredArea">The area that is required.</param>
        /// <param name="iMaxWidth">The maximum width.</param>
        /// <param name="iMaxHeight">The maximum height</param>
        public override void GetNextSizeThatFits(int iRequiredArea, int iMaxWidth, int iMaxHeight)
        {
            m_bSwapWidthHeight = false;
            m_iWidth = 32;
            m_iHeight = 32;
            while ((m_iWidth * m_iHeight < iRequiredArea ||
                    m_iWidth < iMaxWidth || m_iHeight < iMaxHeight) && !HasMaxSize)
            {
                if (m_iHeight > m_iWidth)
                {
                    m_iWidth *= 2;
                }
                else
                {
                    m_iHeight *= 2;
                }
                m_bSwapWidthHeight = !m_bSwapWidthHeight;
            }
        }

        /// <summary>
        /// Returns <c>true</c>.
        /// </summary>
        /// <returns><c>True</c>.</returns>
        public override bool IsPowerOfTwo()
        {
            return true;
        }

        #endregion
    }
}
