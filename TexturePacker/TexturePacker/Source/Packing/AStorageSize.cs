﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 17:03:41

namespace TexturePacker.Packing
{
    /// <summary>
    /// Handles the size of <see cref="ImageStorage"/>s. 
    /// </summary>
    public abstract class AStorageSize
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// Width.
        /// </summary>
        protected int m_iWidth;

        /// <summary>
        /// Width.
        /// </summary>
        protected int m_iHeight;

        /// <summary>
        /// The maximum width or height.
        /// </summary>
        protected int m_iMaxSize;

        /// <summary>
        /// The minimum width or height.
        /// </summary>
        protected int m_iMinSize;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Constructs a new instance with the maximum size. 
        /// </summary>
        /// <param name="iMaxSize">The maximum width or height.</param>
        protected AStorageSize(int iMaxSize)
        {
            m_iWidth = 0;
            m_iHeight = 0;
            m_iMaxSize = iMaxSize;
            m_iMinSize = 32;
        }

        #endregion

        // **************************************************************************
        #region Abstract functions

        /// <summary>
        /// Increments the width and/or height to the next level.
        /// </summary>
        public abstract  void Increment();

        /// <summary>
        /// Decrements the width and/or height to the previous level.
        /// </summary>
        public abstract  void Decrement();

        /// <summary>
        /// Sets the width and height to the smallest sizes that matches the required area.
        /// </summary>
        /// <param name="iRequiredArea">The area that is required.</param>
        /// <param name="iMaxWidth">The maximum width.</param>
        /// <param name="iMaxHeight">The maximum height</param>
        public abstract  void GetNextSizeThatFits(int iRequiredArea, int iMaxWidth, int iMaxHeight);

        /// <summary>
        /// Returns if the width and height are constrained to power-of-two values
        /// </summary>
        /// <returns><c>True</c> if the width and height are constrained to power-of-two values <c>false</c> if not.</returns>
        public abstract  bool IsPowerOfTwo();

        #endregion

        // **************************************************************************
        #region Protected functions

        /// <summary>
        /// Constrains the width and height to be smaller or equal the maximum size.
        /// </summary>
        protected void AdjustMaxSize()
        {
            if (m_iWidth > m_iMaxSize)
                m_iWidth = m_iMaxSize;
            if (m_iHeight > m_iMaxSize)
                m_iHeight = m_iMaxSize;
        }

        /// <summary>
        /// Constrains the width and height to be greater or equal the minimum size.
        /// </summary>
        protected void AdjustMinSize()
        {
            if (m_iWidth < m_iMinSize)
                m_iWidth = m_iMinSize;
            if (m_iHeight < m_iMinSize)
                m_iHeight = m_iMinSize;
        }

        #endregion

        // **************************************************************************
        #region Properties

        /// <summary>
        /// Returns the area (width*height).
        /// </summary>
        public int Area
        {
            get { return m_iWidth*m_iHeight; }
        }

        /// <summary>
        /// The width.
        /// </summary>
        public int Width
        {
            get { return m_iWidth; }
            set { m_iWidth = value; }
        }

        /// <summary>
        /// The height.
        /// </summary>
        public int Height
        {
            get { return m_iHeight; }
            set { m_iHeight = value; }
        }

        /// <summary>
        /// Returns whether the width and the height have the minimum size or not.
        /// </summary>
        public bool HasMinSize
        {
            get { return m_iWidth == m_iMinSize && m_iHeight == m_iMinSize; }
        }

        /// <summary>
        /// Returns whether the width and the height have the maximum size or not.
        /// </summary>
        public bool HasMaxSize
        {
            get { return m_iWidth == m_iMaxSize && m_iHeight == m_iMaxSize; }
        }

        #endregion
    }
}
