﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 17:37:19

using TextureAtlas;

namespace TexturePacker.Packing
{
    /// <summary>
    /// The <see cref="TreeNode"/> represents a node in the binary tree.
    /// </summary>
    public class TreeNode
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// X-Position of the nodes rectangle.
        /// </summary>
        private readonly int m_iX;

        /// <summary>
        /// Y-Position of the nodes rectangle.
        /// </summary>
        private readonly int m_iY;

        /// <summary>
        /// Width of the nodes rectangle.
        /// </summary>
        private readonly int m_iWidth;

        /// <summary>
        /// Height of the nodes rectangle.
        /// </summary>
        private readonly int m_iHeight;

        /// <summary>
        /// Reference to the first child of the node.
        /// </summary>
        private TreeNode m_FirstChild;

        /// <summary>
        /// Reference to the second child of the node.
        /// </summary>
        private TreeNode m_SecondChild;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Constructs a new <see cref="TreeNode"/>.
        /// </summary>
        /// <param name="iX">X-Position of the nodes rectangle.</param>
        /// <param name="iY">Y-Position of the nodes rectangle.</param>
        /// <param name="iWidth">Width of the nodes rectangle.</param>
        /// <param name="iHeight">Height of the nodes rectangle.</param>
        public TreeNode(int iX, int iY, int iWidth, int iHeight)
        {
            m_iX = iX;
            m_iY = iY;
            m_iWidth = iWidth;
            m_iHeight = iHeight;
        }

        #endregion

        // **************************************************************************
        #region Private functions

        private void CreateBranches(AtlasImage image)
        {
            int iDeltaX = m_iWidth - image.Width;
            int iDeltaY = m_iHeight - image.Height;

            // We split to give one very small leaf and one very big one
            // because it allows more efficient use of space
            // if you don't do this, the bottom right corner never gets used
            if (iDeltaX < iDeltaY)
            {
                //	The first child is right next to the image
                m_FirstChild = new TreeNode(m_iX + image.Width, m_iY,
                        iDeltaX, image.Height);

                // The second child is below the image and the first one 
                m_SecondChild = new TreeNode(m_iX, m_iY + image.Height,
                        m_iWidth, iDeltaY);
            }
            else
            {
                //	The first child is below the image
                m_FirstChild = new TreeNode(m_iX, m_iY + image.Height,
                        image.Width, iDeltaY);

                // The second child is to the right of the image and the first child
                m_SecondChild = new TreeNode(m_iX + image.Width, m_iY,
                        iDeltaX, m_iHeight);
            }
        }

        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// Adds an <see cref="AtlasImage"/> to the tree.
        /// </summary>
        /// <param name="image">The <see cref="AtlasImage"/> to add.</param>
        /// <returns>
        /// <c>True</c> if the image fits in this <see cref="TreeNode"/> or one of 
        /// it's children and is successfully added to the tree, <c>false</c> if not.
        /// </returns>
        public bool Add(AtlasImage image)
        {
            if (HasNoChildren())
            {
                if (Fits(image))
                {
                    CreateBranches(image);
                    image.XPos = (short)m_iX;
                    image.YPos = (short)m_iY;

                    return true;
                }
                return false;
            }
            if (m_FirstChild.Add(image))
            {
                return true;
            }
            if (m_SecondChild.Add(image))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Checks if an <see cref="AtlasImage"/> fits inside this <see cref="TreeNode"/>.
        /// </summary>
        /// <param name="image">The <see cref="AtlasImage"/> to check.</param>
        /// <returns><c>True</c> if the <see cref="AtlasImage"/> fits inside this <see cref="TreeNode"/>, <c>false</c> if not.</returns>
        public bool Fits(AtlasImage image)
        {
            return image.Width <= m_iWidth && image.Height <= m_iHeight;
        }

        /// <summary>
        /// Checks if this <see cref="TreeNode"/> got children.
        /// </summary>
        /// <returns>
        /// <c>True</c> if this <see cref="TreeNode"/> has no children, <c>false</c> if it got at least one child.
        /// </returns>
        public bool HasNoChildren()
        {
            return m_FirstChild == null && m_SecondChild == null;
        }

        #endregion
    }
}
