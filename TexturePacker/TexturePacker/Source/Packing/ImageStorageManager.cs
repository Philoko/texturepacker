﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 21:19:53

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TextureAtlas;

namespace TexturePacker.Packing
{
    /// <summary>
    /// The <see cref="ImageStorageManager"/> is used for packing <see cref="AtlasImage"/>s. 
    /// A binary tree bin packing algorithm is used to pack the images in <see cref="ImageStorage"/>s.
    /// If one storage is not enough (depending on the <see cref="ATextureSize"/> a new 
    /// <see cref="ImageStorage"/> is created until all images are packed.
    /// </summary>
    public class ImageStorageManager : IEnumerable<ImageStorage>
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// The <see cref="ImageStorage"/>s of the <see cref="ImageStorageManager"/>.
        /// </summary>
        private readonly List<ImageStorage> m_ImageStorages;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Constructs a new <see cref="ImageStorageManager"/>.
        /// </summary>
        public ImageStorageManager()
        {
            m_ImageStorages = new List<ImageStorage>();
        }

        #endregion

        // **************************************************************************
        #region Private functions

        private void Fill(List<AtlasImage> images)
        {
            // Iterate in reverse order to safely remove the current item while iterating
            for (int i = images.Count - 1; i >= 0; i--)
            {
                if (m_ImageStorages.Any(imageStorage => imageStorage.Add(images[i])))
                {
                    images.RemoveAt(i);
                }
                else
                {
                    // The image couldn't be added to any storage
                    break;
                }
            }
        }

        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// Packs the images in <see cref="ImageStorage"/>s.
        /// </summary>
        /// <param name="images">The <see cref="AtlasImage"/>s to pack.</param>
        /// <param name="storageSize">
        /// The <see cref="AStorageSize"/> is for managing the size of the <see cref="ImageStorage"/>s.
        /// </param>
        public void PackImages(List<AtlasImage> images, AStorageSize storageSize)
        {
            images.Sort(new AtlasImageAreaComparer());
            while (images.Count != 0)
            {
                int iMaxWidth = 0;
                int iMaxHeight = 0;
                int iRequiredArea = 0;
                foreach (AtlasImage atlasImage in images)
                {
                    if (iMaxWidth < atlasImage.Width)
                    {
                        iMaxWidth = atlasImage.Width;
                    }
                    if (iMaxHeight < atlasImage.Height)
                    {
                        iMaxHeight = atlasImage.Height;
                    }
                    iRequiredArea += atlasImage.Width * atlasImage.Height;
                }
                // Calculate the required size of the next storage
                storageSize.GetNextSizeThatFits(iRequiredArea, iMaxWidth, iMaxHeight);

                // Create a new storage with the required size
                ImageStorage imageStorage = new ImageStorage(storageSize.Width,
                        storageSize.Height);

                // Add it to the storage manager
                m_ImageStorages.Add(imageStorage);

                // Fill the images that are left in the storage manager
                Fill(images);

                // Were all images filled in the storage?
                if (images.Count != 0)
                {
                    // Try to increment the size of the last added storage until we reach the maximum size.
                    while (!storageSize.HasMaxSize)
                    {
                        storageSize.Increment();

                        // Remove the storage from the storage manager
                        m_ImageStorages.Remove(imageStorage);

                        // Take back the images in the last storage and resort the list again
                        images.AddRange(imageStorage);
                        images.Sort(new AtlasImageAreaComparer());

                        // Add a new storage with the new size to the storage manager
                        imageStorage = new ImageStorage(storageSize.Width, storageSize.Height);
                        m_ImageStorages.Add(imageStorage);

                        // Try to fill all images
                        Fill(images);

                        if (images.Count == 0)
                        {
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the <see cref="ImageStorage"/> at a specific index.
        /// </summary>
        /// <param name="iIndex">The index of the <see cref="ImageStorage"/>.</param>
        /// <returns></returns>
        public ImageStorage this[int iIndex]
        {
            get
            {
                return m_ImageStorages[iIndex];
            }
        }

        #endregion

        // **************************************************************************
        #region Properties

        /// <summary>
        /// The amount of <see cref="ImageStorage"/>s inside the <see cref="ImageStorageManager"/>.
        /// </summary>
        public int Count
        {
            get { return m_ImageStorages.Count; }
        }

        #endregion

        // **************************************************************************
        #region Implementation of IEnumerable

        /// <summary>
        /// Returns all <see cref="ImageStorage"/>s.
        /// </summary>
        /// <returns>All <see cref="ImageStorage"/>s.</returns>
        public IEnumerator<ImageStorage> GetEnumerator()
        {
            return m_ImageStorages.GetEnumerator();
        }

        /// <summary>
        /// Returns all <see cref="ImageStorage"/>s.
        /// </summary>
        /// <returns>All <see cref="ImageStorage"/>s.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
