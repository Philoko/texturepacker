﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 17:14:53

using System;

namespace TexturePacker.Packing
{
    /// <summary>
    /// An implementation of <see cref="AStorageSize"/> for non-power-of-two sizes.
    /// </summary>
    public class StorageSizeNPot : AStorageSize
    {
        // **************************************************************************
        #region Constants

        /// <summary>
        /// The minimum increment and decrement step.
        /// </summary>
        public const int STEP_SIZE = 1;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Constructs a new instance with the maximum size. 
        /// </summary>
        /// <param name="iMaxSize">The maximum width or height.</param>
        public StorageSizeNPot(int iMaxSize) : base(iMaxSize)
        {

        }

        #endregion

        // **************************************************************************
        #region Overwritten from ATextureSize

        /// <summary>
        /// Increments the width or height to the next level.
        /// </summary>
        public override void Increment()
        {
            if (m_iHeight > m_iWidth)
            {
                m_iWidth += STEP_SIZE;
            }
            else
            {
                m_iHeight += STEP_SIZE;
            }
            AdjustMaxSize();
        }

        /// <summary>
        /// Decrements the width or height to the previous level.
        /// </summary>
        public override void Decrement()
        {
            if (m_iHeight < m_iWidth)
            {
                m_iWidth -= STEP_SIZE;
            }
            else
            {
                m_iHeight -= STEP_SIZE;
            }
            AdjustMinSize();
        }

        /// <summary>
        /// Sets the width and height to the smallest sizes that matches the required area.
        /// </summary>
        /// <param name="iRequiredArea">The area that is required.</param>
        /// <param name="iMaxWidth">The maximum width.</param>
        /// <param name="iMaxHeight">The maximum height</param>
        public override void GetNextSizeThatFits(int iRequiredArea, int iMaxWidth, int iMaxHeight)
        {
            // Some of the smaller images fit into the other textures
            // so choose a smaller size
            int iSize = (int)Math.Round(Math.Sqrt(iRequiredArea));
            if (iSize < 32)
            {
                iSize = 32;
            }
            if (iMaxWidth > iSize)
            {
                m_iWidth = iMaxWidth;
                m_iHeight = iRequiredArea / m_iWidth + 1;
            }
            else
            {
                m_iWidth = iSize;
            }
            if (iMaxHeight > iSize)
            {
                m_iHeight = iMaxHeight;
                m_iWidth = iRequiredArea / m_iHeight + 1;
            }
            else
            {
                m_iHeight = iSize;
            }
            AdjustMaxSize();
        }

        /// <summary>
        /// Return <c>false</c>.
        /// </summary>
        /// <returns><c>False</c>.</returns>
        public override bool IsPowerOfTwo()
        {
            return false;
        }

        #endregion


    }
}
