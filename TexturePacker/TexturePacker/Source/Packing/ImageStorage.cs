﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 18:09:16

using System.Collections;
using System.Collections.Generic;
using TextureAtlas;

namespace TexturePacker.Packing
{
    /// <summary>
    /// An <see cref="ImageStorage"/> is for storing <see cref="AtlasImage"/>s in a tree structure. 
    /// It has an underlying <see cref="AtlasImageContainer"/> which contains all images which are added to the storage.
    /// </summary>
    public class ImageStorage : IEnumerable<AtlasImage>
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// Root of the tree.
        /// </summary>
        private readonly TreeNode m_RootTreeNode;

        /// <summary>
        /// The <see cref="AtlasImageContainer"/> where all images in the tree are stored.
        /// </summary>
        private readonly AtlasImageContainer m_ImageContainer;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Creates a new <see cref="ImageStorage"/>.
        /// </summary>
        /// <param name="iWidth">Initial width.</param>
        /// <param name="iHeight">Initial height.</param>
	    public ImageStorage(int iWidth, int iHeight)
	    {
            m_RootTreeNode = new TreeNode(0, 0, iWidth, iHeight);
            m_ImageContainer = new AtlasImageContainer(iWidth, iHeight);
	    }

        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// Adds an <see cref="AtlasImage"/> to the <see cref="ImageStorage"/>.
        /// </summary>
        /// <param name="image">The <see cref="AtlasImage"/> to add.</param>
        /// <returns>
        /// <c>True</c> if the image fits and was added to the storage, 
        /// <c>false</c> if their is not enough space left in the storage for the passed image.
        /// </returns>
        public bool Add(AtlasImage image)
        {
            if (m_RootTreeNode.Add(image))
            {
                m_ImageContainer.Add(image);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Fills images from the passed list in the storage. It could be that not all images fit in the storage.
        /// Images that are stored will be automatically removed from the list.
        /// </summary>
        /// <param name="images">The list of <see cref="AtlasImage"/>s to be stored in this storage.</param>
        public void Fill(List<AtlasImage> images)
        {
            // Iterate in reverse order to safely remove the current item while iterating
            for (int i = images.Count - 1; i >= 0; i--)
            {
                if (!Add(images[i]))
                {
                    return;
                }
                images.RemoveAt(i);
            }
        }

        #endregion

        // **************************************************************************
        #region Properties

        /// <summary>
        /// All images in this <see cref="ImageStorage"/>.
        /// </summary>
        public IEnumerable Images
        {
            get { return m_ImageContainer; }
        }

        /// <summary>
        /// Gets the underlying <see cref="AtlasImageContainer"/>.
        /// </summary>
        public AtlasImageContainer AtlasImageContainer
        {
            get { return m_ImageContainer; }
        }

        #endregion

        // **************************************************************************
        #region Implementation of IEnumerable

        /// <summary>
        /// All images in this <see cref="ImageStorage"/>.
        /// </summary>
        /// <returns>All images in this <see cref="ImageStorage"/>.</returns>
        public IEnumerator<AtlasImage> GetEnumerator()
        {
            return m_ImageContainer.GetEnumerator();
        }

        /// <summary>
        /// All images in this <see cref="ImageStorage"/>.
        /// </summary>
        /// <returns>All images in this <see cref="ImageStorage"/>.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
