﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 21:00:33

using System.Collections.Generic;
using TextureAtlas;

namespace TexturePacker.Packing
{
    /// <summary>
    /// AreaComparer
    /// </summary>
    public class AtlasImageAreaComparer : IComparer<AtlasImage>
    {
        // **************************************************************************
        #region Implementation of IComparer

        public int Compare(AtlasImage x, AtlasImage y)
        {
            return x.Width*x.Height - y.Width*y.Height;
        }

        #endregion
    }
}
