﻿// Author:  Philipp Okoampah
// Created: 12.11.2014 15:01:00

using System;
using System.IO;
using System.Xml.Serialization;

namespace TexturePacker
{
    /// <summary>
    /// This class contains the Main-Method.
    /// </summary>
    public class TexturePackerMain
    {
        /// <summary>
        /// Entry point of the application.
        /// </summary>
        /// <param name="args">The first argument should contain the path to the XML-Definition file.</param>
        private static void Main(string[] args)
        {
            try
            {
                // Load the options file
                FileInfo buildScript = new FileInfo(args[0]);
                AtlasCreator atlasCreator;
                using (FileStream fileStream = buildScript.OpenRead())
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(AtlasCreator));
                    atlasCreator = (AtlasCreator)serializer.Deserialize(fileStream);
                }
                atlasCreator.Process();        
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(1);
            }          
        }
    }
}
