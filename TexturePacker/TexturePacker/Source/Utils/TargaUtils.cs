﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 10:29:08

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace TexturePacker.Utils
{
    /// <summary>
    /// Utility functions to convert byte-arrays containing Targa-images to <see cref="Bitmap"/>s.
    /// </summary>
    public static class TargaUtils
    {
        // **************************************************************************
        #region Constants

        // Targa format constants
        private const int HEADER_SIZE = 18;
        private const int UNCOMPRESSED_RGB = 2;
        private const int COMPRESSED_RGB = 10;
        private const int UNCOMPRESSED_GREYSCALE = 3;
        private const int COMPRESSED_GREYSCALE = 11;

        // Bits per pixel
        private const int BGRA = 32;
        private const int BGR = 24;
        private const int GREYSCALE = 8;

        // Header indices
        private const int BITS_PER_PIXEL_INDEX = 16;
        private const int IMAGE_TYPE_INDEX = 2;
        private const int WIDTH_UPPER_BITS_INDEX = 13;
        private const int WIDTH_LOWER_BITS_INDEX = 12;
        private const int HEIGHT_UPPER_BITS_INDEX = 15;
        private const int HEIGHT_LOWER_BITS_INDEX = 14;

        #endregion

        // **************************************************************************
        #region Static functions

        /// <summary>
        /// Converts a <see cref="Bitmap"/> to a Targa-image in form of a byte-array.
        /// </summary>
        /// <param name="bitmap">The <see cref="Bitmap"/> to convert.</param>
        /// <param name="ePixelFormat">
        /// The target <see cref="PixelFormat"/>. This must be either <see cref="PixelFormat.Format32bppArgb"/>, 
        /// <see cref="PixelFormat.Format24bppRgb"/> or <see cref="PixelFormat.Format8bppIndexed"/>. Note that 
        /// <see cref="PixelFormat.Format8bppIndexed"/> is only used to specify an 8 bit greyscale image and not an 
        /// indexed image format. Only alpha channel of the bitmap used for the greyscale image.
        /// </param>
        /// <param name="bCompressed">Determines whether the image should be RLE-Compressed or not.</param>
        /// <returns>A byte-array containing the Targa-image.</returns>
        public static byte[] WriteTarga(Bitmap bitmap, PixelFormat ePixelFormat, bool bCompressed)
        {            
            int iBitsPerPixel = 0;
            int iPixelFormat = 0;

            Action<int, BinaryWriter> dWritePixel = null;

            switch (ePixelFormat)
            {
                case PixelFormat.Format32bppArgb:                        
                    iBitsPerPixel = BGRA;
                    iPixelFormat = bCompressed ? COMPRESSED_RGB : UNCOMPRESSED_RGB;
                    dWritePixel = WriteBgraFromIntArgb;
                    break;
                case PixelFormat.Format24bppRgb:
                    iBitsPerPixel = BGR;
                    iPixelFormat = bCompressed ? COMPRESSED_RGB : UNCOMPRESSED_RGB;
                    dWritePixel = WriteBgrFromIntRgb;
                    break;
                case PixelFormat.Format8bppIndexed:
                    iBitsPerPixel = GREYSCALE;
                    iPixelFormat = bCompressed ? COMPRESSED_GREYSCALE : UNCOMPRESSED_GREYSCALE;
                    dWritePixel = WriteGreyscaleFromIntGreyscale;
                    break;
            }

            if (iBitsPerPixel == 0 || iPixelFormat == 0)
            {
                throw new InvalidOperationException("The pixel format \"" + bitmap.PixelFormat + "\" is not supported by this Targa-writer.");
            }

            using (MemoryStream targetStream = new MemoryStream())
            using (BinaryWriter writer = new BinaryWriter(targetStream))
            {
                WriteTargaHeader(bitmap, iBitsPerPixel, iPixelFormat, writer);
                if (bCompressed)
                {
                    WriteCompressed(bitmap, writer, dWritePixel);
                }
                else
                {
                    WriteUncompressed(bitmap, writer, dWritePixel);
                }
                WriteTargaFooter(writer);
                return targetStream.ToArray();
            }
        }

        private static void WriteTargaHeader(Bitmap bitmap, int iBitsPerPixel, int iPixelFormat, BinaryWriter writer)
        {
            writer.Write((byte)0); // No image id
            writer.Write((byte)0); // Marks if a color map exists. We don't support color maps.
            writer.Write((byte)iPixelFormat); // The type of the image
            writer.Write((short)0); // Start position of the color map. We don't support color maps.
            writer.Write((short)0); // Length of the color map. We don't support color maps.
            writer.Write((byte)0); // Bits per color map pixel. We don't support color maps.
            writer.Write((short)0); // Image origin x
            writer.Write((short)0); // Image origin y
            writer.Write((byte)(bitmap.Width & 0xFF)); // Width lower bits
            writer.Write((byte)((bitmap.Width >> 8) & 0xFF)); // Width higher bits
            writer.Write((byte)(bitmap.Height & 0xFF)); // Height lower bits
            writer.Write((byte)((bitmap.Height >> 8) & 0xFF)); // Height higher bits
            writer.Write((byte)iBitsPerPixel); // Bits per pixel
            writer.Write((byte)32); // Bits 3-0 give the alpha channel depth, Bits 5-4 give direction
        }

        private static void WriteUncompressed(Bitmap bitmap, BinaryWriter writer, Action<int, BinaryWriter> dWritePixel)
        {
            int[] aiImageBuffer = new int[bitmap.Height * bitmap.Width];
            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    aiImageBuffer[y * bitmap.Width + x] = bitmap.GetPixel(x, y).ToArgb();
                }
            }
            foreach (int i in aiImageBuffer)
            {
                dWritePixel(i, writer);
            }
        }

        private static void WriteCompressed(Bitmap bitmap, BinaryWriter writer, Action<int, BinaryWriter> dWritePixel)
        {
            int[] aiImageBuffer = new int[bitmap.Height * bitmap.Width];
            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    aiImageBuffer[y*bitmap.Width + x] = bitmap.GetPixel(x, y).ToArgb();
                }
            }

            int iPixelIndex = 0;
            while (iPixelIndex < aiImageBuffer.Length)
            {
                int iPacketStartIndex = iPixelIndex;

                // Count the next consecutive pixels that have the same color values 
                while (iPixelIndex + 1 < aiImageBuffer.Length
                    && aiImageBuffer[iPixelIndex] == aiImageBuffer[iPixelIndex + 1]
                    && iPixelIndex - iPacketStartIndex < 127) // 127 is the maximum of consecutive pixels that can be stored in the RLE header
                {
                    iPixelIndex++;
                }

                // Uncompressed package
                // At least 3 consecutive pixels must be identical to use the RLE compression method
                int iHeader;
                if (iPixelIndex - iPacketStartIndex < 3)
                {
                    // The packet is raw, now we have to count how many more bytes until we reach a sequence of 3
                    while (iPixelIndex + 2 < aiImageBuffer.Length
                        && (aiImageBuffer[iPixelIndex] != aiImageBuffer[iPixelIndex + 1] 
                        || aiImageBuffer[iPixelIndex] != aiImageBuffer[iPixelIndex + 2])
                        && iPixelIndex - iPacketStartIndex < 127) // 127 is the maximum of consecutive pixels that can be stored in the RLE header
                    {
                        iPixelIndex++;
                    }
                    if (iPixelIndex + 2 >= aiImageBuffer.Length)
                    {
                        iPixelIndex = aiImageBuffer.Length;
                    }
                    iHeader = (iPixelIndex - iPacketStartIndex - 1);
                    writer.Write((byte)(iHeader & 0xFF));
                    for (int n = iPacketStartIndex; n < iPixelIndex; n++)
                    {
                        dWritePixel(aiImageBuffer[n], writer);
                    }
                }
                // RLE Compressed package
                else
                {
                    if (iPixelIndex + 1 >= aiImageBuffer.Length)
                    {
                        iPixelIndex = aiImageBuffer.Length - 1;
                    }

                    iHeader = 0x80 | (iPixelIndex - iPacketStartIndex);
                    writer.Write((byte)(iHeader & 0xFF));

                    dWritePixel(aiImageBuffer[iPixelIndex], writer);
                    iPixelIndex++;
                }
            }
        }

        private static void WriteBgraFromIntArgb(int iArgb, BinaryWriter writer)
        {
            writer.Write((byte)(iArgb & 0xFF));
            writer.Write((byte)((iArgb >> 8) & 0xFF));
            writer.Write((byte)((iArgb >> 16) & 0xFF));
            writer.Write((byte)((iArgb >> 24) & 0xFF));
        }
        private static void WriteBgrFromIntRgb(int iRgb, BinaryWriter writer)
        {
            writer.Write((byte)(iRgb & 0xFF));
            writer.Write((byte)((iRgb >> 8) & 0xFF));
            writer.Write((byte)((iRgb >> 16) & 0xFF));
        }
        private static void WriteGreyscaleFromIntGreyscale(int iGreyscale, BinaryWriter writer)
        {
            writer.Write((byte)((iGreyscale >> 24) & 0xFF));
        }

        private static void WriteTargaFooter(BinaryWriter writer)
        {
            // Escape all possible additional meta-informations
            writer.Write("\0\0\0\0\0\0\0\0TRUEVISION-XFILE."); 
        }

        /// <summary>
        /// Converts a byte-array containing a Targa-image to a <see cref="Bitmap"/>.
        /// This works for compressed (RLE) and uncompressed TGAs as well.
        /// </summary>
        /// <param name="aiRawBuffer">Array of bytes containing a Targa-image.</param>
        /// <returns>The converted image as <see cref="Bitmap"/>.</returns>
        public static Bitmap ReadTarga(byte[] aiRawBuffer)
        {
            // Read the width and height from the header
            int iWidth = aiRawBuffer[WIDTH_LOWER_BITS_INDEX] + (aiRawBuffer[WIDTH_UPPER_BITS_INDEX] << 8);
            int iHeight = aiRawBuffer[HEIGHT_LOWER_BITS_INDEX] + (aiRawBuffer[HEIGHT_UPPER_BITS_INDEX] << 8);

            // Read the image type
            int iImageType = aiRawBuffer[IMAGE_TYPE_INDEX];

            // Read the bits per pixel
            int iBitsPerPixel = aiRawBuffer[BITS_PER_PIXEL_INDEX];

            PixelFormat ePixelFormat = PixelFormat.DontCare;
            byte[] aiImageBuffer = null;

            RotateFlipType eRotateFlipType = RotateFlipType.RotateNoneFlipNone;

            switch (iImageType)
            {
                case UNCOMPRESSED_RGB:
                    if (iBitsPerPixel == BGRA)
                    {
                         // uncompressed BGRA
                        aiImageBuffer = LoadUncompressedBgra(iWidth, iHeight, aiRawBuffer);
                        ePixelFormat = PixelFormat.Format32bppArgb;
                    }
                    else if (iBitsPerPixel == BGR)
                    {
                        // uncompressed BGR
                        aiImageBuffer = LoadUncompressedBgr(iWidth, iHeight, aiRawBuffer);
                        ePixelFormat = PixelFormat.Format24bppRgb;
                        eRotateFlipType = RotateFlipType.RotateNoneFlipY;
                    }
                    break;
                case COMPRESSED_RGB:
                    if (iBitsPerPixel == BGRA)
                    {
                         // compressed BGRA
                        aiImageBuffer = LoadCompressedBgra(iWidth, iHeight, aiRawBuffer);
                        ePixelFormat = PixelFormat.Format32bppArgb;
                    }
                    else if (iBitsPerPixel == BGR)
                    {
                        // compressed BGR
                        aiImageBuffer = LoadCompressedBgr(iWidth, iHeight, aiRawBuffer);
                        ePixelFormat = PixelFormat.Format24bppRgb;
                        eRotateFlipType = RotateFlipType.RotateNoneFlipY;
                    }
                    break;
                case UNCOMPRESSED_GREYSCALE:
                    if (iBitsPerPixel == GREYSCALE)
                    {
                         // uncompressed greyscale
                        aiImageBuffer = LoadUncompressedGreyscale(iWidth, iHeight, aiRawBuffer);
                        // .Net is not supporting greyscale channels at the moment
                        ePixelFormat = PixelFormat.Format24bppRgb;
                    }
                    break;
                case COMPRESSED_GREYSCALE:
                    if (iBitsPerPixel == GREYSCALE)
                    {
                        // compressed greyscale
                        aiImageBuffer = LoadCompressedGreyscale(iWidth, iHeight, aiRawBuffer);
                        // .Net is not supporting greyscale channels at the moment
                        ePixelFormat = PixelFormat.Format24bppRgb;
                    }
                    break;
            }

            if (aiImageBuffer == null || ePixelFormat == PixelFormat.DontCare)
            {
                throw new InvalidOperationException("The image format is not supported.");
            }

            // Create the bitmap
            Bitmap bitmap = new Bitmap(iWidth, iHeight, ePixelFormat);
            BitmapData data = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), ImageLockMode.WriteOnly, ePixelFormat);
            Marshal.Copy(aiImageBuffer, 0, data.Scan0, aiImageBuffer.Length);
            bitmap.UnlockBits(data);

            // Rotate the image if necessary
            bitmap.RotateFlip(eRotateFlipType);
            return bitmap;
        }

        private static byte[] LoadUncompressedBgr(int iWidth, int iHeight, byte[] aiRawBuffer)
        {
            int iByteCount = iWidth*iHeight*3;
            byte[] aiImageBuffer = new byte[iByteCount];

            Array.Copy(aiRawBuffer, HEADER_SIZE, aiImageBuffer, 0, iByteCount);

            return aiImageBuffer;
        }

        private static byte[] LoadUncompressedBgra(int iWidth, int iHeight, byte[] aiRawBuffer)
        {
            int iByteCount = iWidth*iHeight*4;
            byte[] aiImageBuffer = new byte[iByteCount];

            Array.Copy(aiRawBuffer, HEADER_SIZE, aiImageBuffer, 0, iByteCount);

            return aiImageBuffer;
        }

        private static byte[] LoadUncompressedGreyscale(int iWidth, int iHeight, byte[] aiRawBuffer)
        {
            int iByteCount = iWidth * iHeight;
            byte[] aiImageBuffer = new byte[iByteCount];

            Array.Copy(aiRawBuffer, HEADER_SIZE, aiImageBuffer, 0, iByteCount);

            return aiImageBuffer;
        }

        private static byte[] LoadCompressedBgr(int iWidth, int iHeight, byte[] aiRawBuffer)
        {
            int iByteCount = iWidth * iHeight * 3;
            byte[] aiImageBuffer = new byte[iByteCount];

            using (MemoryStream sourceStream = new MemoryStream(aiRawBuffer))
            using (BinaryReader reader = new BinaryReader(sourceStream))
            using (MemoryStream targetStream = new MemoryStream(aiImageBuffer))
            using (BinaryWriter writer = new BinaryWriter(targetStream))
            {
                // Ignore the header
                sourceStream.Seek(HEADER_SIZE, SeekOrigin.Begin);
                while (targetStream.Position != iByteCount)
                {
                    int iChunkHeader = reader.ReadByte();
                    if (iChunkHeader < 128) // If The Chunk Is A 'RAW' Chunk
                    {
                        iChunkHeader++; // Add 1 To The Value To Get Total Number Of Raw Pixels
                        // Start pixel reading loop
                        for (short iCounter = 0; iCounter < iChunkHeader; iCounter++)
                        {
                            // Read one pixel
                            writer.Write(reader.ReadByte()); // B                    
                            writer.Write(reader.ReadByte()); // G
                            writer.Write(reader.ReadByte()); // R
                        }
                    }
                    else // If it's an RLE header
                    {
                        iChunkHeader -= 127; // Subtract 127 To Get Rid Of The ID Bit
                        byte b = reader.ReadByte(); // B
                        byte g = reader.ReadByte(); // G
                        byte r = reader.ReadByte(); // R

                        for (short iCounter = 0; iCounter < iChunkHeader; iCounter++)
                        {                                 
                            writer.Write(b); // B                              
                            writer.Write(g); // G                 
                            writer.Write(r); // R                            
                        }
                    }
                }
            }
            return aiImageBuffer;
        }
        private static byte[] LoadCompressedBgra(int iWidth, int iHeight, byte[] aiRawBuffer)
        {
            int iByteCount = iWidth * iHeight * 4;
            byte[] aiImageBuffer = new byte[iByteCount];

            using (MemoryStream sourceStream = new MemoryStream(aiRawBuffer))
            using (BinaryReader reader = new BinaryReader(sourceStream))
            using (MemoryStream targetStream = new MemoryStream(aiImageBuffer))
            using (BinaryWriter writer = new BinaryWriter(targetStream))
            {
                // Ignore the header
                sourceStream.Seek(HEADER_SIZE, SeekOrigin.Begin);
                while (targetStream.Position != iByteCount)
                {
                    int iChunkHeader = reader.ReadByte();
                    if (iChunkHeader < 128) // If The Chunk Is A 'RAW' Chunk
                    {
                        iChunkHeader++; // Add 1 To The Value To Get Total Number Of Raw Pixels
                        // Start pixel reading loop
                        for (short iCounter = 0; iCounter < iChunkHeader; iCounter++)
                        {
                            // Read one pixel
                            writer.Write(reader.ReadByte()); // B
                            writer.Write(reader.ReadByte()); // G           
                            writer.Write(reader.ReadByte()); // R                    
                            writer.Write(reader.ReadByte()); // A    
                        }
                    }
                    else // If it's an RLE header
                    {
                        iChunkHeader -= 127; // Subtract 127 To Get Rid Of The ID Bit
                        byte b = reader.ReadByte(); // B
                        byte g = reader.ReadByte(); // G
                        byte r = reader.ReadByte(); // R
                        byte a = reader.ReadByte(); // A

                        for (short iCounter = 0; iCounter < iChunkHeader; iCounter++)
                        {                               
                            writer.Write(b); // B                            
                            writer.Write(g); // G                         
                            writer.Write(r); // R 
                            writer.Write(a); // A                                                          
                        }
                    }
                }
            }
            return aiImageBuffer;
        }

        private static byte[] LoadCompressedGreyscale(int iWidth, int iHeight, byte[] aiRawBuffer)
        {
            int iByteCount = iWidth * iHeight * 3;
            byte[] aiImageBuffer = new byte[iByteCount];

            using (MemoryStream sourceStream = new MemoryStream(aiRawBuffer))
            using (BinaryReader reader = new BinaryReader(sourceStream))
            using (MemoryStream targetStream = new MemoryStream(aiImageBuffer))
            using (BinaryWriter writer = new BinaryWriter(targetStream))
            {
                // Ignore the header
                sourceStream.Seek(HEADER_SIZE, SeekOrigin.Begin);
                while (targetStream.Position != iByteCount)
                {
                    int iChunkHeader = reader.ReadByte();
                    if (iChunkHeader < 128) // If The Chunk Is A 'RAW' Chunk
                    {
                        iChunkHeader++; // Add 1 To The Value To Get Total Number Of Raw Pixels
                        // Start pixel reading loop
                        for (short iCounter = 0; iCounter < iChunkHeader; iCounter++)
                        {
                            // Read one pixel
                            byte a = reader.ReadByte();
                            writer.Write(a); // A
                            writer.Write(a); // A
                            writer.Write(a); // A
                        }
                    }
                    else // If it's an RLE header
                    {
                        iChunkHeader -= 127; // Subtract 127 To Get Rid Of The ID Bit
                        byte a = reader.ReadByte(); // A

                        for (short iCounter = 0; iCounter < iChunkHeader; iCounter++)
                        {
                            writer.Write(a); // A
                            writer.Write(a); // A
                            writer.Write(a); // A                         
                        }
                    }
                }
            }
            return aiImageBuffer;
        }
        #endregion
    }
}
