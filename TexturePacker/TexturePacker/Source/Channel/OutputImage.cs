﻿// Author:  Philipp Okoampah
// Created: 01.12.2014 15:21:23

using System.Xml.Serialization;

namespace TexturePacker.Channel
{
    /// <summary>
    /// Definition of an output image of the <see cref="AtlasCreator"/>
    /// </summary>
    public class OutputImage
    {
        // **************************************************************************
        #region Properties

        /// <summary>
        /// Name of the output image(s).
        /// </summary>
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Color channel(s) of the output image(s). Only "RGB", "RGBA" and "A" are allowed.
        /// </summary>
        [XmlAttribute(AttributeName = "ColorChannel")]
        public EColorChannel ColorChannel { get; set; }

        #endregion
    }
}
