﻿// Author:  Philipp Okoampah
// Created: 01.12.2014 15:20:11

using System.Xml.Serialization;

namespace TexturePacker.Channel
{
    /// <summary>
    /// Definition of an input image of the <see cref="AtlasCreator"/>
    /// </summary>
    public class InputImage
    {
        // **************************************************************************
        #region Properties

        /// <summary>
        /// Name of the input image.
        /// </summary>
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Path of the folder which contains the input images.
        /// </summary>
        [XmlAttribute(AttributeName = "Folder")]
        public string Path { get; set; }

        #endregion
    }
}
