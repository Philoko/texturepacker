﻿// Author:  Philipp Okoampah
// Created: 10.11.2014 14:57:40

using System;
using System.Collections.Generic;

namespace TexturePacker.Channel
{
    /// <summary>
    /// Struct for handling color channels.
    /// </summary>
    [Flags]
    public enum EColorChannel : byte
    {
        RED = 1,
        GREEN = 2,
        BLUE = 4,
        ALPHA = 8,
        R = RED,
        G = GREEN,
        B = BLUE,
        A = ALPHA,
        RG = R | G,
        RB = R | B,
        RA = R | A,
        GB = G | B,
        GA = G | A,
        BA = B | A,
        RGB = R | G | B,
        RGBA = R | G | B | A,
    };

    /// <summary>
    /// Utility functions for the <see cref="EColorChannel"/> struct.
    /// </summary>
    public static class ColorChannelUtils
    {
        // **************************************************************************
        #region Extensions

        /// <summary>
        /// Converts a <see cref="EColorChannel"/> to a string.
        /// </summary>
        /// <param name="eColorChannel">The <see cref="EColorChannel"/> to convert.</param>
        /// <returns>The <see cref="EColorChannel"/> as string.</returns>
        public static string GetFileRepresentation(this EColorChannel eColorChannel)
        {
            switch (eColorChannel)
            {
                case EColorChannel.RED:
                    return "red";
                case EColorChannel.GREEN:
                    return "green";
                case EColorChannel.BLUE:
                    return "blue";
                case EColorChannel.ALPHA:
                    return "alpha";
                case EColorChannel.RG:
                    return "RG";
                case EColorChannel.RB:
                    return "RB";
                case EColorChannel.RA:
                    return "RA";
                case EColorChannel.GB:
                    return "GB";
                case EColorChannel.GA:
                    return "GA";
                case EColorChannel.BA:
                    return "BA";
                case EColorChannel.RGB:
                    return "RGB";
                case EColorChannel.RGBA:
                    return "RGBA";
                default:
                    throw new ArgumentOutOfRangeException("eColorChannel");
            }
        }

        /// <summary>
        /// Returns a list of single <see cref="EColorChannel"/>s from a combined <see cref="EColorChannel"/>.
        /// </summary>
        /// <param name="eColorChannel"></param>
        /// <returns></returns>
        public static List<EColorChannel> GetSingleChannels(this EColorChannel eColorChannel)
        {
            List<EColorChannel> singleChannels = new List<EColorChannel>();
            for (int i = 0; i < 4; i++)
            {
                if (((int)eColorChannel & (1 << i)) != 0)
                {
                    singleChannels.Add((EColorChannel)(1 << i));
                }
            }
            return singleChannels;
        }

        #endregion

        // **************************************************************************
        #region Static functions

        /// <summary>
        /// Returns a <see cref="EColorChannel"/> for a string.
        /// </summary>
        /// <param name="sFileRepresentation">The string.</param>
        /// <returns>The <see cref="EColorChannel"/>.</returns>
        public static EColorChannel GetColorChannelForFileRepresentation(string sFileRepresentation)
        {
            sFileRepresentation = sFileRepresentation.ToLower();
            switch (sFileRepresentation)
            {
                case "red":
                case "R":
                case "r":
                    return EColorChannel.RED;
                case "green":
                case "G":
                case "g":
                    return EColorChannel.GREEN;
                case  "blue":
                case "B":
                case "b":
                    return EColorChannel.BLUE;
                case "alpha":
                case "A":
                case "a":
                    return EColorChannel.ALPHA;
                case "RG":
                case "rg":
                    return EColorChannel.RG;
                case "RB":
                case "rb":
                    return EColorChannel.RB;
                case "RA":
                case "ra":
                    return EColorChannel.RA;
                case "GB":
                case "gb":
                    return EColorChannel.GB;
                case "GA":
                case "ga":
                    return EColorChannel.GA;
                case "BA":
                case "ba":
                    return EColorChannel.BA;
                case "RGB":
                case "rgb":
                    return EColorChannel.RGB;
                case "RGBA":
                case "rgba":
                    return EColorChannel.RGBA;
                default:
                    throw new ArgumentOutOfRangeException("sFileRepresentation");
            }
        }

        /// <summary>
        /// Returns the bit offset for a bit-shift-operation.
        /// </summary>
        /// <param name="eColorChannel">The <see cref="EColorChannel"/>. Only the single channel values 'R', 'G', 'B' and 'A' are allowed.</param>
        /// <returns>The number of bits that must be shifted to the left.</returns>
        /// <remarks>
        /// <exception cref="ArgumentOutOfRangeException">If <c>eChannel</c> is not one of the values 'R', 'G', 'B' or 'A'.</exception>
        /// <example>
        /// <code>
        /// int iColor = System.Drawing.Color.Aqua;
        /// byte iRed = (byte)((iColor >> GetColorChannelBitOffset(EColorChannel.Red)) & 0xFF)
        /// </code>
        /// </example>
        /// </remarks>
        private static int GetColorChannelBitOffset(EColorChannel eColorChannel)
        {
            switch (eColorChannel)
            {
                case EColorChannel.RED:
                    return 16;
                case EColorChannel.GREEN:
                    return 8;
                case EColorChannel.BLUE:
                    return 0;
                case EColorChannel.ALPHA:
                    return 24;
                default:
                    throw new ArgumentOutOfRangeException("eColorChannel", "Only the single channel values 'R', 'G', 'B' and 'A' are allowed.");
            }
        }

        /// <summary>
        /// Returns a single channel as byte for a int color value of the form ARGB.
        /// </summary>
        /// <param name="iColor">The color value.</param>
        /// <param name="eChannel">The <see cref="EColorChannel"/>. Only the single channel values 'R', 'G', 'B' and 'A' are allowed.</param>
        /// <returns>The single color channel value as byte.</returns>
        /// <exception cref="ArgumentOutOfRangeException">If <c>eChannel</c> is not one of the values 'R', 'G', 'B' or 'A'.</exception>
        public static byte GetSingleChannelOfColor(int iColor, EColorChannel eChannel)
        {
            return (byte)((iColor >> GetColorChannelBitOffset(eChannel)) & 0xFF);
        }

        /// <summary>
        /// Changes a single channel of a color to a new value.
        /// </summary>
        /// <param name="iColor">The color.</param>
        /// <param name="iChannelValue">The value of the single color channel.</param>
        /// <param name="eChannel">The <see cref="EColorChannel"/>. Only the single channel values 'R', 'G', 'B' and 'A' are allowed.</param>
        /// <returns>The new color with the changed channel.</returns>
        /// <exception cref="ArgumentOutOfRangeException">If <c>eChannel</c> is not one of the values 'R', 'G', 'B' or 'A'.</exception>
        public static int SetSingleChannelInColor(int iColor, byte iChannelValue, EColorChannel eChannel)
        { 
            // Get the channel offset
            int iColorChannelOffset = GetColorChannelBitOffset(eChannel);

            // Clear the channel
            iColor &= ~(0xFF << iColorChannelOffset);

            // Set the new value
            iColor |= iChannelValue << iColorChannelOffset;

            return iColor;
        }

        #endregion
    }
}