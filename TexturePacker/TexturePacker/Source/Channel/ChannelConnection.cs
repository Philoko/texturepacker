﻿// Author:  Philipp Okoampah
// Created: 10.11.2014 15:16:16

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml.Serialization;

namespace TexturePacker.Channel
{
    /// <summary>
    /// Connects <see cref="EColorChannel"/> of an <see cref="InputImage"/> with <see cref="EColorChannel"/> of an <see cref="OutputImage"/>.
    /// </summary>
    public class ChannelConnection
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// The <see cref="EColorChannel"/> of the input image.
        /// </summary>
        private EColorChannel m_eInputColorChannel;

        /// <summary>
        /// The <see cref="EColorChannel"/> of the input image.
        /// </summary>
        private EColorChannel m_eOutputColorChannel;

        /// <summary>
        /// A RGB color key to set for fully transparent pixels.
        /// </summary>
        private Color m_ColorKey;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ChannelConnection()
        {
            m_ColorKey = default(Color);
        }

        #endregion

        // **************************************************************************
        #region Private functions

        private int WritePixelChannel(int iSourcePixel, EColorChannel eSourceChannel, int iTargetPixel,
            EColorChannel eTargetChannel)
        {
            byte iSourceChannelValue = ColorChannelUtils.GetSingleChannelOfColor(iSourcePixel, eSourceChannel);
            return ColorChannelUtils.SetSingleChannelInColor(iTargetPixel, iSourceChannelValue, eTargetChannel);
        }

        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// Takes the color channels of the <paramref name="sourceImage"/> and writes them to the <paramref name="targetImage"/>.
        /// </summary>
        /// <param name="sourceImage">The source image to take the color information from.</param>
        /// <param name="targetImage">The target image to write the color information.</param>
        /// <param name="iColorKeyAlphaThreshhold">
        /// Determines a threshold when an alpha value is considered to be fully transparent, and the optional color key is applied.
        /// </param>
        /// <exception cref="Exception">If no target or source <see cref="EColorChannel"/>s are specified.</exception>
        /// <exception cref="InvalidOperationException">
        /// When the channel connection is invalid (e.g. many source channels for one target channels are specified).
        /// </exception>
        public void Write(Bitmap sourceImage, Bitmap targetImage, int iColorKeyAlphaThreshhold)
        {
            // SourceChannel -> TargetChannel
            Dictionary<EColorChannel, EColorChannel> singleConnections = new Dictionary<EColorChannel, EColorChannel>();

            // List all single color channels 
            List<EColorChannel> sourceSingleChannels = m_eInputColorChannel.GetSingleChannels();
            List<EColorChannel> targetSingleChannels = m_eOutputColorChannel.GetSingleChannels();

            // No color channels
            if (sourceSingleChannels.Count == 0 || sourceSingleChannels.Count == 0)
            {
                throw new Exception("No source, or no target channel has been specified.");
            }

            // It is a one to one function
            if (sourceSingleChannels.Count == targetSingleChannels.Count)
            {
                for (int i = 0; i < sourceSingleChannels.Count; i++)
                {
                    singleConnections.Add(sourceSingleChannels[i], targetSingleChannels[i]);
                }
            }                
            
            /*// One source channel for many target channels
            else if (sourceSingleChannels.Count == 1)
            {
                for (int i = 0; i < targetSingleChannels.Count; i++)
                {
                    singleConnections.Add(sourceSingleChannels[0], targetSingleChannels[i]);
                }
            }*/

            // Something else
            else
            {
                throw new InvalidOperationException("Can't use the channel(s) \"" + m_eInputColorChannel +
                                    " \" as source(s) and the channel(s) \" " + m_eOutputColorChannel +
                                    "\" as target(s).");
            }

            for (int y = 0; y < sourceImage.Height; y++)
            {
                for (int x = 0; x < sourceImage.Width; x++)
                {
                    foreach (KeyValuePair<EColorChannel, EColorChannel> singleConnection in singleConnections)
                    {
                        Color sourceColor = sourceImage.GetPixel(x, y);
                        if (m_ColorKey != default(Color) && sourceColor.A < iColorKeyAlphaThreshhold)
                        {
                            targetImage.SetPixel(x, y, m_ColorKey);
                            continue;
                        }

                        int iPixel = WritePixelChannel(sourceImage.GetPixel(x, y).ToArgb(), singleConnection.Key,
                            targetImage.GetPixel(x, y).ToArgb(), singleConnection.Value);
                        targetImage.SetPixel(x, y, Color.FromArgb(iPixel));
                    }
                }
            }
        }
        #endregion

        // **************************************************************************
        #region Properties

        /// <summary>
        /// The name of the <see cref="InputImage"/> of this <see cref="ChannelConnection"/>.
        /// </summary>
        [XmlAttribute(AttributeName = "InputImage")]
        public string InputImageName { get; set; }

        /// <summary>
        /// The name of the <see cref="OutputImage"/> of this <see cref="ChannelConnection"/>.
        /// </summary>
        [XmlAttribute(AttributeName = "OutputImage")]
        public string OutputImageName { get; set; }

        /// <summary>
        /// The input <see cref="EColorChannel"/>s.
        /// </summary>
        [XmlAttribute(AttributeName = "InputColor")]
        public EColorChannel InputColorChannel
        {
            get { return m_eInputColorChannel; }
            set { m_eInputColorChannel = value; }
        }

        /// <summary>
        /// The output <see cref="EColorChannel"/>s.
        /// </summary>
        [XmlAttribute(AttributeName = "OutputColor")]
        public EColorChannel OutputColorChannel
        {
            get { return m_eOutputColorChannel; }
            set { m_eOutputColorChannel = value; }
        }

        /// <summary>
        /// A RGB color key for fully transparent pixels.
        /// </summary>
        [XmlAttribute]
        public string ColorKey
        {
            get { return ColorTranslator.ToHtml(m_ColorKey); }
            set { m_ColorKey = ColorTranslator.FromHtml(value); }
        }

        #endregion
    }
}
