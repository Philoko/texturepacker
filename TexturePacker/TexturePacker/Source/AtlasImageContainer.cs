﻿// Author:  Philipp Okoampah
// Created: 09.11.2014 18:14:15

using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using TextureAtlas;

namespace TexturePacker
{
    /// <summary>
    /// This used during creation of an texture atlas. It contains a list of <see cref="AtlasImage"/>s and it's own size.
    /// All the rectangles of the listed <see cref="AtlasImage"/>s should fit inside the width and height of the <see cref="AtlasImageContainer"/>.
    /// </summary>
    public class AtlasImageContainer : IEnumerable<AtlasImage>
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// The <see cref="AtlasImage"/>s inside this <see cref="AtlasImageContainer"/>.
        /// </summary>
        private readonly List<AtlasImage> m_Images;

        /// <summary>
        /// Name of the <see cref="AtlasImageContainer"/>.
        /// </summary>

        private string m_sName;

        /// <summary>
        /// The width.
        /// </summary>
        private int m_iWidth;

        /// <summary>
        /// The height.
        /// </summary>
        private int m_iHeight;

        /// <summary>
        /// The <see cref="Bitmap"/> all <see cref="AtlasImage"/>s of this <see cref="AtlasImageContainer"/> are rendered on.
        /// </summary>
        private Bitmap m_Bitmap;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Creates a new <see cref="AtlasImageContainer"/> with an initial width and height.
        /// </summary>
        /// <param name="iWidth">The initial width.</param>
        /// <param name="iHeight">The initial height</param>
        public AtlasImageContainer(int iWidth, int iHeight)
        {
            m_iWidth = iWidth;
            m_iHeight = iHeight;
            m_Images = new List<AtlasImage>();
            m_Bitmap = null;
        }

        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// Adds an <see cref="AtlasImage"/> to the <see cref="AtlasImageContainer"/>.
        /// </summary>
        /// <param name="image">The <see cref="AtlasImage"/> to add.</param>
        public void Add(AtlasImage image)
        {
            m_Images.Add(image);
        }

        /// <summary>
        /// Trims the dimensions of this <see cref="AtlasImageContainer"/> to an minimum width and height.
        /// </summary>
        public void TrimDimensions()
        {
            m_iWidth = 0;
            m_iHeight = 0;
            foreach (AtlasImage atlasImage in m_Images)
            {
                if (m_iWidth < atlasImage.Right)
                    m_iWidth = atlasImage.Right;
                if (m_iHeight < atlasImage.Bottom)
                    m_iHeight = atlasImage.Bottom;
            }
        }

        /// <summary>
        /// Sets a new size.
        /// </summary>
        /// <param name="iWidth">The new width.</param>
        /// <param name="iHeight">The new height.</param>
        public void SetSize(int iWidth, int iHeight)
        {
            m_iWidth = iWidth;
            m_iHeight = iHeight;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get { return m_sName; }
            set { m_sName = value; }
        }

        #endregion

        // **************************************************************************
        #region Properties

        /// <summary>
        /// Gets the height.
        /// </summary>
        public int Height
        {
            get { return m_iHeight; }
        }

        /// <summary>
        /// Gets the width.
        /// </summary>
        public int Width
        {
            get { return m_iWidth; }
        }

        /// <summary>
        /// Gets the <see cref="AtlasImage"/>s
        /// </summary>
        public IEnumerable<AtlasImage> Images
        {
            get
            {   
                return m_Images;
            }
        }

        /// <summary>
        /// Gets the area (width*height).
        /// </summary>
        public int Area
        {
            get { return m_iWidth*m_iHeight; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Bitmap"/> of this <see cref="AtlasImageContainer"/>.
        /// </summary>
        public Bitmap Bitmap
        {
            get { return m_Bitmap; }
            set { m_Bitmap = value; }
        }

        #endregion

        // **************************************************************************
        #region Implementation of IEnumerable

        /// <summary>
        /// Returns all <see cref="AtlasImage"/>s.
        /// </summary>
        /// <returns>All <see cref="AtlasImage"/>s</returns>
        public IEnumerator<AtlasImage> GetEnumerator()
        {
            return m_Images.GetEnumerator();
        }

        /// <summary>
        /// Returns all <see cref="AtlasImage"/>s.
        /// </summary>
        /// <returns>All <see cref="AtlasImage"/>s</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
