﻿// Author:  Philipp Okoampah
// Created: 07.11.2014 17:30:17

using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace TextureAtlas
{
    /// <summary>
    /// The <c>Atlas</c> stores <see cref="AtlasImage"/> and the name of the texture(s) which contains the image data.
    /// </summary>
    /// <remarks>
    /// Note that one <c>Atlas</c> can have multiple real textures. The names of the textures are stored in a 
    /// dictionary with texture type names as keys.
    /// </remarks>
    public class Atlas : IXmlSerializable, IEnumerable<AtlasImage>
    {
        // **************************************************************************
        #region Constants

        private const string MATERIAL_ELEMENT_NAME = "Material";
        private const string TEXTURES_ELEMENT_NAME = "Textures";
        private const string TEXTURE_ELEMENT_NAME = "Texture";
        private const string TEXTURE_NAME_ATTRIBUTE = "Name";
        private const string TEXTURE_FILE_NAME_ATTRIBUTE = "FileName";
        private const string IMAGES_ELEMENT_NAME = "Images";
        private const string IMAGE_ELEMENT_NAME = "Image";

        #endregion

        // **************************************************************************
        #region Member variables

        /// <summary>
        /// References to physical textures.
        /// </summary>
        private readonly Dictionary<string, string> m_Textures;

        /// <summary>
        /// All images this <c>Atlas</c> contains.
        /// </summary>
        private readonly List<AtlasImage> m_AtlasImages; 

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Constructs a new <c>Atlas</c>
        /// </summary>
        public Atlas()
        {
            m_Textures = new Dictionary<string, string>();
            m_AtlasImages = new List<AtlasImage>();
        }
                     
        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// Adds a <see cref="AtlasImage"/> to this <c>Atlas</c>. This should only happen during creation of an <c>Atlas</c>.
        /// </summary>
        /// <param name="atlasImage">The <see cref="AtlasImage"/> to be added.</param>
        public void AddImage(AtlasImage atlasImage)
        {
            m_AtlasImages.Add(atlasImage);
        }

        /// <summary>
        /// Adds a texture to the dictionary of textures. This should only happen during creation of an <c>Atlas</c>.
        /// </summary>
        /// <param name="sTypeName">The name of the texture type.</param>
        /// <param name="sFileName">The filename of the texture.</param>
        public void AddTexture(string sTypeName, string sFileName)
        {
            m_Textures.Add(sTypeName, sFileName);
        }

        /// <summary>
        /// Returns a texture filename for the type as key.
        /// </summary>
        /// <param name="sTypeName">The typename.</param>
        /// <returns>The filename of the texture for the given typename.</returns>
        public string GetTexture(string sTypeName)
        {
            return m_Textures[sTypeName];
        }

        /// <summary>
        /// Returns if this <c>Altas</c> has a texture for the given type.
        /// </summary>
        /// <param name="sTypeName">The typename. </param>
        /// <returns><c>True</c> if a texture of this type exists, <c>false</c> if not.</returns>
        public bool HasTexture(string sTypeName)
        {
            return m_Textures.ContainsKey(sTypeName);
        }

        #endregion

        // **************************************************************************
        #region Properties

        /// <summary>
        /// The name of the material of this <c>Atlas</c>. This is a user defined value. It can be used 
        /// to determine which textures this <c>Atlas</c> has or should have.
        /// </summary>
        public string Material { get; set; }

        /// <summary>
        /// Returns all <see cref="AtlasImage"/> as <see cref="IEnumerable{T}"/>.
        /// </summary>
        public IEnumerable<AtlasImage> AtlasImages
        {
            get { return m_AtlasImages; }
        }

        /// <summary>
        /// Returns all texture file names of this <c>Atlas</c>.
        /// </summary>
        public IEnumerable<string> TextureNames
        {
            get { return m_Textures.Values; }
        }

        /// <summary>
        /// Returns all texture types of this <c>Atlas</c>.
        /// </summary>
        public IEnumerable<string> TextureTypes
        {
            get { return m_Textures.Keys; }
        } 

        #endregion

        // **************************************************************************
        #region Implementation of IXmlSerializable

        /// <summary>
        ///   This method is reserved and should not be used. When implementing the IXmlSerializable interface, you 
        ///   should return null (Nothing in Visual Basic) from this method, and instead, if specifying a custom schema is required, apply the
        ///   <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute" />
        ///   to the class.
        /// </summary>
        /// <returns>
        ///   An <see cref="T:System.Xml.Schema.XmlSchema" /> that describes the XML representation of the object that is produced by the
        ///   <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)" />
        ///   method and consumed by the
        ///   <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)" />
        ///   method.
        /// </returns>
        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }


        /// <summary>
        ///   Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">
        ///   The <see cref="T:System.Xml.XmlReader" /> stream from which the object is deserialized.
        /// </param>   
        public void ReadXml(XmlReader reader)
        {
            XmlSerializer atlasImagesSerializer = new XmlSerializer(typeof (AtlasImage),
                new XmlRootAttribute(IMAGE_ELEMENT_NAME));
            XmlSerializer stringSerializer = new XmlSerializer(typeof (string),
                new XmlRootAttribute(MATERIAL_ELEMENT_NAME));

            bool bWasEmpty = reader.IsEmptyElement;
            reader.Read();

            if (bWasEmpty)
            {
                return;
            }

            Material = (string)stringSerializer.Deserialize(reader);            
            
            reader.ReadStartElement(TEXTURES_ELEMENT_NAME);
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                reader.MoveToContent();
                string sTextureName = reader.GetAttribute(TEXTURE_NAME_ATTRIBUTE);
                string sTextureFileName = reader.GetAttribute(TEXTURE_FILE_NAME_ATTRIBUTE);
                if (sTextureName == null || sTextureFileName == null)
                {
                    throw new XmlException("The \"" + TEXTURE_NAME_ATTRIBUTE + "\"-attribute or the \"" +
                                           TEXTURE_FILE_NAME_ATTRIBUTE + "\"-attribute of the element \"" +
                                           TEXTURE_ELEMENT_NAME + "\" has not been specified.");
                }
                m_Textures.Add(sTextureName, sTextureFileName);
                reader.Read();
            }
            reader.ReadEndElement(); //</TEXTURES_ELEMENT_NAME>

            reader.ReadStartElement(IMAGES_ELEMENT_NAME);
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                m_AtlasImages.Add((AtlasImage)atlasImagesSerializer.Deserialize(reader));
                reader.MoveToContent();
            }
            reader.ReadEndElement(); //</IMAGES_ELEMENT_NAME>
            reader.ReadEndElement(); //</TEXTURE_ATLAS_ELEMENT_NAME>

            foreach (AtlasImage image in m_AtlasImages)
            {
                image.SetAtlas(this);
            }
        }

        /// <summary>
        ///   Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">
        ///   The <see cref="T:System.Xml.XmlWriter" /> stream to which the object is serialized.
        /// </param>
        public void WriteXml(XmlWriter writer)
        {
            XmlSerializer atlasImagesSerializer = new XmlSerializer(typeof (AtlasImage),
                new XmlRootAttribute(IMAGE_ELEMENT_NAME));
            XmlSerializer stringSerializer = new XmlSerializer(typeof (string),
                new XmlRootAttribute(MATERIAL_ELEMENT_NAME));

            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);

            stringSerializer.Serialize(writer, Material, xns);

            writer.WriteStartElement(TEXTURES_ELEMENT_NAME);
            foreach (KeyValuePair<string, string> texture in m_Textures)
            {
                writer.WriteStartElement(TEXTURE_ELEMENT_NAME);
                writer.WriteAttributeString(TEXTURE_NAME_ATTRIBUTE, texture.Key);
                writer.WriteAttributeString(TEXTURE_FILE_NAME_ATTRIBUTE, texture.Value);
                writer.WriteEndElement();//</TEXTURE_ELEMENT_NAME>
            }
            writer.WriteEndElement(); //</TEXTURES_ELEMENT_NAME>

            writer.WriteStartElement(IMAGES_ELEMENT_NAME);
            foreach (AtlasImage image in m_AtlasImages)
            {
                atlasImagesSerializer.Serialize(writer, image, xns);
            }
            writer.WriteEndElement(); //</IMAGES_ELEMENT_NAME>
        }
        #endregion

        // **************************************************************************
        #region Implementation of IEnumerable

        /// <summary>
        /// Returns all <see cref="AtlasImage"/>s/>.
        /// </summary>
        public IEnumerator<AtlasImage> GetEnumerator()
        {
            return m_AtlasImages.GetEnumerator();
        }

        /// <summary>
        /// Returns all <see cref="AtlasImage"/>s/>.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
