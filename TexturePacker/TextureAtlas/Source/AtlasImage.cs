﻿// Author:  Philipp Okoampah
// Created: 07.11.2014 17:30:53

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TextureAtlas
{
    /// <summary>
    /// Represents a single image inside a <see cref="Atlas"/>. 
    /// </summary>
    /// <remarks>
    /// It stores the x- and y-position of the image inside the textures as well as the width and height of 
    /// the original image this <c>AtlasImage</c> based on together with the x- and y- offset from the left and top of the 
    /// original image. 
    /// </remarks>
    public class AtlasImage : IComparable<AtlasImage>
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// The <see cref="Atlas"/> this <c>AtlasImage</c> belongs to.
        /// </summary>
        private Atlas m_TextureAtlas;

        #endregion

        // **************************************************************************
        #region Internal functions

        /// <summary>
        /// Sets the <see cref="Atlas"/> this <c>AtlasImage</c> belongs to.
        /// </summary>
        /// <param name="atlas">The <see cref="Atlas"/> this <c>AtlasImage</c> should belong to.</param>
        internal void SetAtlas(Atlas atlas)
        {
            m_TextureAtlas = atlas;
        }

        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// Returns the name of the texture of this <c>AtlasImage</c> for a given texture type.
        /// </summary>
        /// <param name="sType">The texture type as atring.</param>
        /// <returns>The name of the texture.</returns>
        public string GetTexture(string sType)
        {
            return m_TextureAtlas.GetTexture(sType);
        }

        /// <summary>
        /// Returns the true if this <c>AtlasImage</c> has a texture for the given texture type.
        /// </summary>
        /// <param name="sType">The texture type as atring.</param>
        /// <returns><c>True</c> if the <c>AtlasImage</c> has a texture for the given type, <c>false</c> otherwise.</returns>
        public bool HasTexture(string sType)
        {
            return m_TextureAtlas.HasTexture(sType);
        }

        #endregion

        // **************************************************************************
        #region Properties

        /// <summary>
        /// Name of this <c>AtlasImage</c>. This is normally the name of the original image without the file-extension.
        /// </summary>
        [XmlAttribute]
        public string ImageName { get; set; }

        /// <summary>
        /// The x-position of the <c>AtlasImage</c> inside the texture(s).
        /// </summary>
        [XmlAttribute]
        public short XPos { get; set; }

        /// <summary>
        /// The x-position of the <c>AtlasImage</c> inside the texture(s).
        /// </summary>
        [XmlAttribute]
        public short YPos { get; set; }

        /// <summary>
        /// The width of the <c>AtlasImage</c> inside the texture(s).
        /// </summary>
        [XmlAttribute]
        public short Width { get; set; }

        /// <summary>
        /// The width of the <c>AtlasImage</c> inside the texture(s).
        /// </summary>
        [XmlAttribute]
        public short Height { get; set; }

        /// <summary>
        /// The left offset of the <c>AtlasImage</c> to its original image.
        /// </summary>
        [XmlAttribute]
        public short XOffset { get; set; }

        /// <summary>
        /// The upper offset of the <c>AtlasImage</c> to its original image.
        /// </summary>
        [XmlAttribute]
        public short YOffset { get; set; }

        /// <summary>
        /// The width of the original image this <c>AtlasImage</c> based on.
        /// </summary>
        [XmlAttribute]
        public short OriginalWidth { get; set; }

        /// <summary>
        /// The height of the original image this <c>AtlasImage</c> based on.
        /// </summary>
        [XmlAttribute]
        public short OriginalHeight { get; set; }

        /// <summary>
        /// Returns true if the <c>AtlasImage</c> ha a x- or y-offset.
        /// </summary>
        [XmlIgnore]
        public bool HasOffset
        {
            get { return XOffset != 0 || YOffset != 0; }
        }

        /// <summary>
        /// Returns the y-position plus the height.
        /// </summary>
        [XmlIgnore]
        public int Bottom
        {
            get { return YPos + Height; }
        }

        /// <summary>
        /// Returns the x-position plus the width.
        /// </summary>
        [XmlIgnore]
        public int Right
        {
            get { return XPos + Width; }
        }

        /// <summary>
        /// Returns all texture file names of this <c>AtlasImage</c>.
        /// </summary>
        [XmlIgnore]
        public IEnumerable<string> TextureNames
        {
            get { return m_TextureAtlas.TextureNames; }
        }

        /// <summary>
        /// Returns all texture types of this <c>AtlasImage</c>.
        /// </summary>
        [XmlIgnore]
        public IEnumerable<string> TextureTypes
        {
            get { return m_TextureAtlas.TextureTypes; }
        } 

        #endregion

        // **************************************************************************
        #region Implementation of IComparable

        /// <summary>
        /// Just compares the names of two <c>AtlasImage</c>s with culture independend ordinal comparsion.
        /// </summary>
        /// <param name="other">The other <c>AtlasImage</c> to compare this <c>AtlasImage</c> with.</param>
        /// <returns>The result of the string-comparsion.</returns>
        public int CompareTo(AtlasImage other)
        {
            return String.Compare(ImageName, other.ImageName, StringComparison.Ordinal);
        }

        #endregion
    }
}
