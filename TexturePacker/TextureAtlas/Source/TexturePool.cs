﻿// Author:  Philipp Okoampah
// Created: 14.08.2014 09:28:40

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TextureAtlas
{
    /// <summary>
    /// Just a dictionary for <see cref="AtlasImage"/>s. It is also capable of loading 
    /// an <see cref="Atlas"/> from an XML-file and add all it's <see cref="AtlasImage"/>s to the 
    /// <see cref="TexturePool"/>.
    /// </summary>
    public class TexturePool : IEnumerable<AtlasImage>
    {
        // **************************************************************************
        #region Member variables

        /// <summary>
        /// Dictionary containing all <see cref="AtlasImage"/>s of the <see cref="TexturePool"/>.
        /// </summary>
        private readonly Dictionary<string, AtlasImage> m_TextureAtlasImages;

        /// <summary>
        /// Serializer to deserialize <see cref="Atlas"/>es.
        /// </summary>
        private readonly XmlSerializer m_AtlasSerializer;

        #endregion

        // **************************************************************************
        #region Constructors

        /// <summary>
        /// Constructs a new <see cref="TexturePool"/>.
        /// </summary>
        public TexturePool()
        {
            m_TextureAtlasImages = new Dictionary<string, AtlasImage>();
            m_AtlasSerializer = new XmlSerializer(typeof(Atlas));
        }

        #endregion

        // **************************************************************************
        #region Private functions

        private Atlas LoadAtlas(Stream stream)
        {
            return (Atlas)m_AtlasSerializer.Deserialize(stream);
        }

        #endregion

        // **************************************************************************
        #region Public functions

        /// <summary>
        /// Returns an <see cref="AtlasImage"/> for a given name.
        /// </summary>
        /// <param name="sName">The name of the <see cref="AtlasImage"/> to return.</param>
        /// <returns>The <see cref="AtlasImage"/>.</returns>
        /// <exception cref="ArgumentException">
        /// If an <see cref="AtlasImage"/> with the given name does not exist inside the <see cref="TexturePool"/>.
        /// </exception>
        public AtlasImage GetImage(string sName)
        {
            AtlasImage image;
            if (!m_TextureAtlasImages.TryGetValue(sName, out image))
            {
                throw new ArgumentException("An image with the name \"" + sName + "\" could not be found.");
            }
            return image;
        }

        /// <summary>
        /// Returns an <see cref="AtlasImage"/> for a given name.
        /// </summary>
        /// <param name="sName">The name of the <see cref="AtlasImage"/> to return.</param>
        /// <param name="textureAtlasImage">The <see cref="AtlasImage"/> as out-parameter.</param>
        /// <returns><c>True</c> if an <see cref="AtlasImage"/> with the given was found, <c>false</c> if not.</returns>
        public bool TryGetImage(string sName, out AtlasImage textureAtlasImage)
        {
           return m_TextureAtlasImages.TryGetValue(sName, out textureAtlasImage);
        }

        /// <summary>
        /// Loads an <see cref="Maze.TextureAtlas"/> from a <see cref="Stream"/>. All images of the loaded 
        /// <see cref="Maze.TextureAtlas"/> are automatically added to the pool.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> containing a XML-file of the <see cref="TextureAtlas"/>.</param>
        public void LoadTextureAtlas(Stream stream)
        {
            Atlas textureAtlas = LoadAtlas(stream);
            AddAllImages(textureAtlas);
        }

        /// <summary>
        /// Adds an <see cref="AtlasImage"/> to the <see cref="TexturePool"/>.
        /// </summary>
        /// <param name="atlasImage">The <see cref="AtlasImage"/> to add.</param>
        /// <exception cref="ArgumentException">
        /// If an <see cref="AtlasImage"/> with the same name already exists in the <see cref="TexturePool"/>.
        /// </exception>
        public void AddImage(AtlasImage atlasImage)
        {
            try
            {
                m_TextureAtlasImages.Add(atlasImage.ImageName, atlasImage);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("An image with the name \"" + atlasImage.ImageName +
                                    "\" should be added to the pool, but an image with this name already exists.");
            }
        }

        /// <summary>
        /// Adds all <see cref="AtlasImage"/>s to the pool.
        /// </summary>
        /// <param name="atlasImages">The <see cref="AtlasImage"/>s as <see cref="IEnumerable{T}"/>.</param>
        /// <exception cref="ArgumentException">If two images have the same name.</exception>
        public void AddAllImages(IEnumerable<AtlasImage> atlasImages)
        {
            foreach (AtlasImage atlasImage in atlasImages)
            {
                try
                {
                    m_TextureAtlasImages.Add(atlasImage.ImageName, atlasImage);
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("An image with the name \"" + atlasImage.ImageName +
                                        "\" should be added to the pool, but an image with this name already exists.");
                }
            }
        }

        #endregion

        // **************************************************************************
        #region Properties

        /// <summary>
        /// Returns the amount of <see cref="AtlasImage"/>s inside the <see cref="TexturePool"/>.
        /// </summary>
        public int Count
        {
            get { return m_TextureAtlasImages.Count; }
        }

        #endregion

        // **************************************************************************
        #region Implementation of IEnumerable

        /// <summary>
        /// Returns all <see cref="AtlasImage"/>s of the <see cref="TexturePool"/>.
        /// </summary>
        /// <returns>All <see cref="AtlasImage"/>s of the <see cref="TexturePool"/>.</returns>
        public IEnumerator<AtlasImage> GetEnumerator()
        {
            return m_TextureAtlasImages.Values.GetEnumerator();
        }

        /// <summary>
        /// Returns all <see cref="AtlasImage"/>s of the <see cref="TexturePool"/>.
        /// </summary>
        /// <returns>All <see cref="AtlasImage"/>s of the <see cref="TexturePool"/>.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
